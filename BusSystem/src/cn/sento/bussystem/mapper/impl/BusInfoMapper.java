package cn.sento.bussystem.mapper.impl;

import java.sql.ResultSet;

import cn.sendto.bussystem.dbhelper.IRowMapper;
import cn.sendto.bussystem.model.BusInfoModel;
/**
 * 
 * ClassName: BusInfoMapper <br/> 
 * Function:数据库中值的传递 <br/> 
 * date: 2017年3月21日 上午10:52:32 <br/> 
 * 
 * @author classA-43 
 * @version  
 * @since JDK 1.8
 */
public class BusInfoMapper implements IRowMapper<BusInfoModel> {

	@Override
	public BusInfoModel rowMapper(ResultSet rs) throws Exception {
		BusInfoModel  bm=new BusInfoModel();
		bm.setId(rs.getInt("id"));
		bm.setBus_name(rs.getString("bus_name"));
		bm.setFirst_station(rs.getString("first_station"));
		bm.setLast_station(rs.getString("last_station"));
		bm.setFirst_bus(rs.getString("first_bus"));
		bm.setLast_bus(rs.getString("last_bus"));
		bm.setStations_num(rs.getInt("stations_num"));
		bm.setTime_interval(rs.getString("time_interval"));
		bm.setLine_length(rs.getDouble("line_length"));
		bm.setCompany(rs.getString("company"));
		bm.setCarfare(rs.getString("carfare"));
		bm.setStop_name(rs.getString("stop_name"));
		
		
		return bm;
	}

}
