package cn.sento.bussystem.mapper.impl;

import java.sql.ResultSet;

import cn.sendto.bussystem.dbhelper.IRowMapper;
import cn.sendto.bussystem.model.StopModel;

public class StopMapper implements IRowMapper<StopModel> {

	@Override
	public StopModel rowMapper(ResultSet rs) throws Exception {
		StopModel s = new StopModel();
		s.setStop_no(rs.getInt("stop_no"));
		s.setBusId(rs.getInt("busId"));
		s.setStop_name(rs.getString("stop_name"));
		return s;
	}

}
