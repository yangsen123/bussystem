package cn.sento.bussystem.mapper.impl;

import java.sql.ResultSet;

import cn.sendto.bussystem.dbhelper.IRowMapper;
import cn.sendto.bussystem.model.AdminModel;

public class AdminMapper implements IRowMapper<AdminModel>{

	@Override
	public AdminModel rowMapper(ResultSet rs) throws Exception {
		AdminModel a = new AdminModel();
		a.setId(rs.getInt("id"));
		a.setAdmin_number(rs.getString("admin_number"));
		a.setAdmin_name(rs.getString("admin_name"));
		a.setAdmin_address(rs.getString("admin_address"));
		a.setAdmin_email(rs.getString("admin_email"));
		a.setAdmin_IdNumber(rs.getString("admin_IdNumber"));
		a.setAdmin_sex(rs.getString("admin_sex"));
		a.setAdmin_pass(rs.getString("admin_pass"));
		return a;
	}

}
