package cn.sento.bussystem.mapper.impl;

import java.sql.ResultSet;

import cn.sendto.bussystem.dbhelper.IRowMapper;
import cn.sendto.bussystem.model.BusModel;

public class BusMapper implements IRowMapper<BusModel> {
	@Override
	public BusModel rowMapper(ResultSet rs) throws Exception {
		BusModel  bm=new BusModel();
		bm.setId(rs.getInt("id"));
		bm.setBus_name(rs.getString("bus_name"));
		bm.setFirst_station(rs.getString("first_station"));
		bm.setLast_station(rs.getString("last_station"));
		bm.setFirst_bus(rs.getString("first_bus"));
		bm.setLast_bus(rs.getString("last_bus"));
		bm.setStations_num(rs.getInt("stations_num"));
		bm.setTime_interval(rs.getString("time_interval"));
		bm.setLine_length(rs.getDouble("line_length"));
		bm.setCompany(rs.getString("company"));
		bm.setCarfare(rs.getString("carfare"));
		return bm;
	}

}
