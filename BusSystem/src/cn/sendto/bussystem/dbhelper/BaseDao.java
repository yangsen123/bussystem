package cn.sendto.bussystem.dbhelper;


public abstract class BaseDao {
  protected DBHelper dbhelper = new DBHelper();
  /**
   * begins:(开启事务). <br/>
   * @author 阿森 
   * @throws Exception 
   */
  public  void begins() throws Exception{
	  dbhelper.begins();
	  
  }
  /**
   * commit:(提交事务). <br/>
   * @author 阿森 
   * @throws Exception 
   */
  public void commit() throws Exception{
	  dbhelper.commit();
  }
  /**
   * rollback:(回滚事务). <br/>
   * @author 阿森 
   * @throws Exception 
   */
  public void rollback() throws Exception{
	 try{ dbhelper.rollback();}
	 catch(Exception e){
	 }
	 
  }
  /**
   * close:(关闭). <br/>
   * @author 阿森  
   * @since JDK 1.8
   */
  public void close(){
	  dbhelper.close();
  }
}