package cn.sendto.bussystem.dbhelper;

import java.sql.ResultSet;

/**
 * 
 * ClassName: IRowMapper <br/> 
 * Function: IRowMapper <br/> 
 * date: 2017年5月7日 下午1:12:23 <br/> 
 * 
 * @author 阿森 
 * @version @param <T> 
 * @since JDK 1.8
 */
public interface IRowMapper<T> {
   public abstract T rowMapper(ResultSet rs) throws Exception;
}
