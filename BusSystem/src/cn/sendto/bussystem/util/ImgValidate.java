package cn.sendto.bussystem.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ImgValidate {
	/**
	 * 
	 * makeImage:形成图片验证码. <br/>
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 * @since JDK 1.8
	 */
	public static void makeImage(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 禁止浏览缓存随机图片
		response.setDateHeader("Expires", -1);
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Pragma", "no-cache");
		// 通知客户机以图片方式打开发送过去的数据
		response.setHeader("Content-Type", "image/jpeg");
		// 在内存中创建一张图片
				// 创建一个带透明色的BufferedImage对象
		BufferedImage image = new BufferedImage(80, 30, BufferedImage.TYPE_INT_RGB);
		// 向图片上写数据
		Graphics g = image.getGraphics(); // 基础面板
		// 设置背景色
		g.setColor(Color.white);
		g.fillRect(0, 0, 80, 30); // 第一个参数背景色左距离，背景色顶部距离，第三个参数是背景色的宽度，第四个是背景色底部
		// 设置写入数据的颜色和字体
		g.setColor(Color.BLACK);
		g.setFont(new Font(null, Font.BOLD, 20));
		String num = new ImgValidate().makeNum();
		request.getSession().setAttribute("checkcode", num); // 把图形验证码存储在session中
		// 方便以后检验
		g.drawString(num, 0, 30); // 第二个参数是左边距离，第三个参数到顶部距离单位像素
		// 把写好的数据的图片输出给浏览器
		ImageIO.write(image, "jpg", response.getOutputStream());

	}

	/**
	 * 
	 * makeNum:(形成随机数). <br/>
	 * 
	 * @author classA-43 
	 * @return 
	 * @since JDK 1.8
	 */
	private String makeNum() {
		Random r = new Random();
		String num = r.nextInt(99999) + ""; // 产生随机六位数字
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < 6 - num.length(); i++) {
			buffer.append("0");
		}
		num = buffer.toString() + num;
		return num;
	}
}
