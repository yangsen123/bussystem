package cn.sendto.bussystem.dao;

import java.sql.SQLException;
import java.util.List;

import cn.sendto.bussystem.dbhelper.BaseDao;
import cn.sendto.bussystem.model.BusInfoModel;
import cn.sendto.bussystem.model.BusModel;
import cn.sento.bussystem.mapper.impl.BusInfoMapper;
import cn.sento.bussystem.mapper.impl.BusMapper;
/**
 * 
 * ClassName: BusDao <br/> 
 * Function: 公交车基本信息相关 <br/> 
 * date: 2017年5月7日 下午12:53:22 <br/> 
 * 
 * @author 阿森 
 * @version  
 * @since JDK 1.8
 */
public class BusDao extends BaseDao {
	/**
	 * 
	 * findBusInfoById:(根据ID查找公交车信息). <br/>
	 * 
	 * @author 阿森 
	 * @param id
	 * @return
	 * @throws Exception 
	 * @since JDK 1.8
	 */
	public List<BusModel> findBusInfoById(String id) throws Exception {
		String sql = "SELECT * FROM bus_info WHERE id=?";
		List<BusModel> list = null;
		try {
			list = dbhelper.executeQuery(sql, new BusMapper(), id);
		} catch (Exception e) {
			throw new RuntimeException();
		}
		return list;
	}

	/**
	 * 
	 * findBusName:(查找所有公交车信息). <br/>
	 * 
	 * @author 阿森 
	 * @return
	 * @throws Exception 
	 * @since JDK 1.8
	 */
	public List<BusModel> findBusName() throws Exception {
		String sql = "SELECT * FROM bus_info";
		List<BusModel> list = null;
		try {
			list = dbhelper.executeQuery(sql, new BusMapper());
		} catch (Exception e) {
			throw new RuntimeException();
		}
		return list;
	}

	/**
	 * 
	 * addBusInfo:(添加公交车信息). <br/>
	 * 
	 * @author 阿森 
	 * @param busModel
	 * @return
	 * @throws Exception 
	 * @since JDK 1.8
	 */
	public int addBusInfo(BusModel busModel) throws Exception {
		int i = 0;
		StringBuffer sql = new StringBuffer();
		sql.append(" INSERT INTO bus_info(bus_name,first_station,last_station,first_bus,");
		sql.append(" last_bus,stations_num,time_interval,line_length,company,carfare)");
		sql.append(" VALUE (?,?,?,?,?,?,?,?,?,?)");
		try {
			i = dbhelper.executeSave(sql.toString(), busModel.getBus_name(), busModel.getFirst_station(),
					busModel.getLast_station(), busModel.getFirst_bus(), busModel.getLast_bus(),
					busModel.getStations_num(), busModel.getTime_interval(), busModel.getLine_length(),
					busModel.getCompany(), busModel.getCarfare());
		} catch (SQLException e) {
			throw new RuntimeException();
		}
		return i;
	}

	/**
	 * 
	 * deleteBusInfo:删除公交车信息). <br/>
	 * 
	 * @author 阿森 
	 * @param id
	 * @return
	 * @throws Exception 
	 * @since JDK 1.8
	 */
	public int deleteBusInfo(String id) throws Exception {
		int i = 0;
		String sql = "DELETE FROM bus_info WHERE id =?";
		try {
			i = dbhelper.executeUpdate(sql, id);
		} catch (SQLException e) {
			throw new RuntimeException();
		}
		return i;
	}

	/**
	 * 
	 * updateBusInfo:(更新公交车数据). <br/>
	 * 
	 * @author 阿森 
	 * @param busInfo
	 * @return
	 * @throws Exception 
	 * @since JDK 1.8
	 */

	public int updateBusInfo(BusModel busInfo) throws Exception {
		int i = 0;
		StringBuffer sql = new StringBuffer();
		sql.append(" UPDATE bus_info SET bus_name=? , first_station=? , ");
		sql.append("  last_station=? , first_bus=? ,last_bus=? , stations_num=?,  ");
		sql.append(" time_interval=?, line_length =? , company=?,carfare=? WHERE  id=?  ");
		try {
			i = dbhelper.executeUpdate(sql.toString(), busInfo.getBus_name(), busInfo.getFirst_station(),
					busInfo.getLast_station(), busInfo.getFirst_bus(), busInfo.getLast_bus(), busInfo.getStations_num(),
					busInfo.getTime_interval(),busInfo.getLine_length(),busInfo.getCompany(),busInfo.getCarfare(),busInfo.getId());
		} catch (SQLException e) {
			throw new RuntimeException();
		}
		return i;
	}
	
	
	/**
	 * 
	 * findBusInfoByName:(根据车辆编号查找公交车信息). <br/>
	 * 
	 * @author 阿森 
	 * @param busName
	 * @return
	 * @throws Exception 
	 * @since JDK 1.8
	 */
	public List<BusInfoModel> findBusInfoByName(String busName) throws Exception {
		busName="%"+busName+"%";
		String sql = "SELECT * FROM bus_info,stops WHERE bus_info.id=stops.stop_no AND bus_name LIKE  ?";
		List<BusInfoModel> list = null;
		try {
			list = dbhelper.executeQuery(sql, new BusInfoMapper(),busName);
		} catch (Exception e) {
			throw new RuntimeException();
		}
		return list;
	}
	/**
	 * 
	 * findStopByStopName:(查找站点信息根据站点名称). <br/>
	 * 
	 * @author 阿森 
	 * @param stopName
	 * @return
	 * @throws Exception 
	 * @since JDK 1.8
	 */
	public List<BusInfoModel> findStopByStopName(String stopName)throws Exception{
		stopName="%"+stopName+"%";
		List<BusInfoModel> list=null;
		String sql = "SELECT * FROM bus_info,stops WHERE  bus_info.id=stops.stop_no AND stop_name LIKE ?";
		try {
			list = dbhelper.executeQuery(sql, new BusInfoMapper(), stopName);
		} catch (Exception e) {
			throw new RuntimeException();
		}
		return list;
		
	}
	
	
}