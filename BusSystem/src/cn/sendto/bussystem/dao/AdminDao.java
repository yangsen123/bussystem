package cn.sendto.bussystem.dao;

import java.sql.SQLException;
import java.util.List;


import cn.sendto.bussystem.dbhelper.BaseDao;
import cn.sendto.bussystem.model.AdminModel;
import cn.sento.bussystem.mapper.impl.AdminMapper;
@SuppressWarnings("all")
public class AdminDao extends BaseDao {
/**
 * 
 * findAllUser:(查找所有用户). <br/>
 * 
 * @author 阿森 
 * @return
 * @throws Exception 
 * @since JDK 1.8
 */
	public List<AdminModel> findAllUser() throws Exception {
		List<AdminModel> list = null;
		String sql = "SELECT * FROM admin ";
		try {
			list = dbhelper.executeQuery(sql, new AdminMapper(), null);
		} catch (Exception e) {
			throw new RuntimeException();
		}
		return list;
	}

	/**
	 * 
	 * login:(登录). <br/>
	 * 
	 * @author 阿森 
	 * @param username
	 * @param password
	 * @return
	 * @throws Exception 
	 * @since JDK 1.8
	 */
	public List<AdminModel> login(String username, String password) throws Exception {
		String sql = "SELECT * FROM admin WHERE admin_number=?  and admin_pass=?";
		List<AdminModel> list = null;
		try {
			list = dbhelper.executeQuery(sql, new AdminMapper(), username, password);
		} catch (Exception e) {
			throw new RuntimeException();
		}
		return list;
	}

	/**
	 * 
	 * addUser:(添加用户). <br/>
	 * 
	 * @author 阿森 
	 * @param adminModel
	 * @return i
	 * @throws Exception 
	 * @since JDK 1.8
	 */
	public int addUser(AdminModel adminModel) throws Exception {
		int i = 0;
		String sql = "INSERT INTO admin(admin_number,admin_pass,admin_name,admin_sex,admin_IdNumber,admin_email,admin_address) VALUES (?,?,?,?,?,?,?)";
		try {
			i = dbhelper.executeSave(sql, adminModel.getAdmin_number(), adminModel.getAdmin_pass(),
					adminModel.getAdmin_name(), adminModel.getAdmin_sex(), adminModel.getAdmin_IdNumber(),
					adminModel.getAdmin_email(), adminModel.getAdmin_address());
		} catch (SQLException e) {
			throw new RuntimeException();
		}
		return i;
	}

	/**
	 * 
	 * deleteUser:(删除用户). <br/>
	 * 
	 * @author 阿森 
	 * @param id
	 * @return
	 * @throws Exception 
	 * @since JDK 1.8
	 */
	public int deleteUser(String id) throws Exception {
		int i = 0;
		String sql = "DELETE FROM admin WHERE id =?";
		try {
			i = dbhelper.executeUpdate(sql, id);
		} catch (SQLException e) {
			throw new RuntimeException();
		}
		return i;
	}

	/**
	 * 
	 * updateAdmin:(更新管理员信息). <br/>
	 * 
	 * @author 阿森 
	 * @param adminModel
	 * @return
	 * @throws Exception 
	 * @since JDK 1.8
	 */
	public int updateAdmin(AdminModel adminModel) throws Exception {
		String sql = "UPDATE admin SET admin_number=?,admin_pass=? ,admin_name=? ,admin_sex=?,admin_IdNumber=? ,admin_email=?,admin_address=? WHERE id=?";
		int i = 0;
		Object[] objs = new Object[] { adminModel.getAdmin_number(), adminModel.getAdmin_pass(),
				adminModel.getAdmin_name(), adminModel.getAdmin_sex(), adminModel.getAdmin_IdNumber(),
				adminModel.getAdmin_email(), adminModel.getAdmin_address(), adminModel.getId() };
		try {
			i = dbhelper.executeUpdate(sql, objs);
		} catch (Exception e) {
			throw new RuntimeException();
		}
		return i;
	}
	/**
	 * 
	 * findAllUserById:(根据ID查找用户). <br/>
	 * 
	 * @author 阿森 
	 * @param id
	 * @return
	 * @throws Exception 
	 * @since JDK 1.8
	 */
	public List<AdminModel> findAllUserById(String id) throws Exception {
		List<AdminModel> list = null;
		String sql = "SELECT * FROM admin where id=? ";
		try {
			list = dbhelper.executeQuery(sql, new AdminMapper(), id);
		} catch (Exception e) {
			throw new RuntimeException();
		}
		return list;
	}
	/**
	 * 
	 * updatePassword:(更新密码). <br/>
	 * 
	 * @author 阿森 
	 * @param password
	 * @param username
	 * @return
	 * @throws Exception 
	 * @since JDK 1.8
	 */
	public int updatePassword(String password, String username)  throws Exception{
		int i = 0;
		String sql = "UPDATE admin SET admin_pass=?  WHERE admin_number=? ";
		try {
			i = dbhelper.executeUpdate(sql, password, username);
		} catch (SQLException e) {
			throw new RuntimeException();
		}
		return i;
	}

}
