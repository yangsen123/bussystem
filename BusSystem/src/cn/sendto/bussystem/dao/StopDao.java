package cn.sendto.bussystem.dao;

import java.sql.SQLException;
import java.util.List;

import cn.sendto.bussystem.dbhelper.BaseDao;
import cn.sendto.bussystem.model.BusInfoModel;
import cn.sendto.bussystem.model.StopModel;
import cn.sento.bussystem.mapper.impl.BusInfoMapper;
import cn.sento.bussystem.mapper.impl.StopMapper;
@SuppressWarnings("all")
/**
 * 
 * ClassName: StopDao <br/> 
 * Function: 站点信息相关 <br/> 
 * date: 2017年5月7日 下午12:57:27 <br/> 
 * 
 * @author 阿森 
 * @version  
 * @since JDK 1.8
 */
public class StopDao extends BaseDao{
	public List<StopModel> findAllStop(String busId){
		List<StopModel> list=null;
		String sql = "SELECT * FROM stops WHERE busId=?";
		try {
			list = dbhelper.executeQuery(sql, new StopMapper(), busId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	/**
	 * 
	 * deleteStop:(删除站点信息). <br/>
	 * 
	 * @author 阿森 
	 * @param id
	 * @return
	 * @throws Exception 
	 * @since JDK 1.8
	 */
	public int deleteStop(String id)throws Exception {
		int i = 0 ;
		String sql = "DELETE FROM stops WHERE busId =?";
		try {
			i =dbhelper.executeUpdate(sql, id);
		} catch (SQLException e) {
			throw new RuntimeException();
		}
		return i;
	}
	/**
	 * 
	 * addStop:(添加站点). <br/>
	 * 
	 * @author 阿森 
	 * @param stop
	 * @return
	 * @throws Exception 
	 * @since JDK 1.8
	 */
	public int addStop(StopModel stop)throws Exception{
		String sql = "INSERT INTO stops (busId,stop_name) VALUES(?,?)";
		int i = 0;
		try {
			i = dbhelper.executeSave(sql, stop.getBusId(),stop.getStop_name());
		} catch (SQLException e) {
			throw new RuntimeException();
		}
		return i;
	}
	/**
	 * 
	 * findStopById:(通过ID查找站点). <br/>
	 * 
	 * @author 阿森 
	 * @param busId
	 * @return 
	 * @since JDK 1.8
	 */
	public List<StopModel> findStopById(int busId){
		List<StopModel> list=null;
		String sql = "SELECT *FROM stops WHERE  busId=?";
		try {
			list = dbhelper.executeQuery(sql, new StopMapper(), busId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
		
	}
	
}
