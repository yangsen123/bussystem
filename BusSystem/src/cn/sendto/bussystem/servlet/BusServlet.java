package cn.sendto.bussystem.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.sendto.bussystem.model.BusModel;
import cn.sendto.bussystem.model.StopModel;
import cn.sendto.bussystem.service.BusService;
import cn.sendto.bussystem.service.StopService;

@WebServlet("/BusServlet")
/**
 * 
 * ClassName: BusServlet <br/>
 * Function:添加站点 先查询数据库中的公交，在根据公交车编号添加站点 <br/>
 * date: 2017年3月28日 上午11:23:41 <br/>
 * 
 * @author classA-43
 * @version
 * @since JDK 1.8
 */
public class BusServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		String id = request.getParameter("id");
		if(id!=null&&id!=""){
		BusService busService = new BusService();
		StopService stopService=new StopService();
		List<BusModel> list = busService.findBusInfoById(id);
		 List<StopModel> sList=stopService.findStopById(list.get(0).getId());
		 String num="";
		 if(sList!=null&&sList.size()>0){
			 num="0";
		 }else{
			 num = String.valueOf(list.get(0).getStations_num());
		 }
		PrintWriter out = response.getWriter();
		out.write(num);
		out.flush();
		out.close();
		}else{
			BusService busService = new BusService();
			List<BusModel> list = busService.findBusName();
			if (list.size() > 0) {
				request.setAttribute("list", list);
			}
			request.getRequestDispatcher("background/manager/addStop.jsp").forward(request, response);
		}
	}

}
