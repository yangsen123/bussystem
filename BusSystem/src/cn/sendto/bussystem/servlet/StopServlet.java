package cn.sendto.bussystem.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.sendto.bussystem.model.StopModel;
import cn.sendto.bussystem.service.StopService;
@WebServlet("/stopServlet")
public class StopServlet extends HttpServlet {

	
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String busId = request.getParameter("id");
		StopService stopService = new StopService();
		List< StopModel> list=stopService.findAllStop(busId);
		String[] alist=null;
		if(list!=null&&list.size()>0){
			alist= list.get(0).getStop_name().split(",");
		}
		if(list!=null&&list.size()>0){
			request.setAttribute("stopList",alist);
		}
		request.getRequestDispatcher("background/manager/see_stop.jsp").forward(request, response);
	}

}
