package cn.sendto.bussystem.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.sendto.bussystem.model.AdminModel;
import cn.sendto.bussystem.service.AdminService;
@WebServlet("/adminServlet")
public class AdminServlet extends HttpServlet {

	private static final long serialVersionUID = -6045038925891782115L;


	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	this.doPost(request, response);
	}


	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	AdminService adminService = new AdminService();
	List<AdminModel> list = null;
	list =adminService.findAllUser();
	if(list != null && list.size() > 0){
		request.setAttribute("admin", list);
	}
	request.getRequestDispatcher("background/manager/admin_list.jsp").forward(request, response);
	}

}
