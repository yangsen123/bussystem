/**
 * 
 */
package cn.sendto.bussystem.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.sendto.bussystem.util.ImgValidate;

/**
 * 
 * ClassName: ImgUtil <br/> 
 * Function: 验证码 <br/> 
 * date: 2017年5月7日 下午1:21:38 <br/> 
 * 
 * @author 阿森 
 * @version  
 * @since JDK 1.8
 */
@WebServlet("/imgUtil")
public class ImgUtil extends HttpServlet {

	
	/** 
	 * serialVersionUID:TODO(��һ�仰�������������ʾʲô). 
	 * @since JDK 1.8 
	 */  
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ImgValidate.makeImage(request, response);
		
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		response.setContentType("text/html;charset=utf-8");
		//从Session里面拿到验证码
		String checkCode =(String) request.getSession().getAttribute("checkcode");
		String code = request.getParameter("validate");
		PrintWriter out = response.getWriter();
		if(null==code||"".equals(code)){
		   out.print("请输入验证码");
		}else{
			  if(code.equals(checkCode)){
				  out.print("");
			  }else{
				  out.print("验证码输入有误");
			  }
			
		}
	}
		
	

	

}
