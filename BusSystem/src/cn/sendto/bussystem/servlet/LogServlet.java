package cn.sendto.bussystem.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import cn.sendto.bussystem.model.AdminModel;
import cn.sendto.bussystem.service.AdminService;

@WebServlet("/logServlet")
public class LogServlet extends HttpServlet {


	private static final long serialVersionUID = 1L;
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  request.setCharacterEncoding("UTF-8");
			String type= request.getParameter("method");
			if (null != type && "" != type) {
			if(type.equals("in")){
				this.login(request, response);
				}else if(type.equals("out")){
					this.logOut(request, response);
				}else if(type.equals("register")){
					this.addUser(request, response);
				}else  if(type.equals("getAdminInfo")){
					this.getAdminInfoByID(request, response);
				}else  if(type.equals("changePass")){
					this.changeAdminPass(request, response);
				}
			}
	}
/**
 * 
 * login:(登录). <br/>
 * 
 * @author classA-43 
 * @param request
 * @param response
 * @throws ServletException
 * @throws IOException 
 * @since JDK 1.8
 */
	protected void login(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		AdminService adminService = new AdminService();
		List<AdminModel> list = null;
		if (username != null && username != "" && password != "" && password != null) {
			list = adminService.login(username, password);
			if (list.size() > 0) {
				request.getSession().setAttribute("username", username);
				request.getSession().setAttribute("password", password);
				response.sendRedirect("/BusSystem/background/index.jsp");
			} else {
				response.sendRedirect("/BusSystem/background/manager/login.jsp");
			}
		}

	}
/**
 * 
 * logOut:��ȫ�˳� <br/>
 * 
 * @author classA-43 
 * @param request
 * @param response
 * @throws ServletException
 * @throws IOException 
 * @since JDK 1.8
 */
	protected void logOut(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getSession().removeAttribute("username");
		request.getSession().removeAttribute("adminName");
		response.sendRedirect("/BusSystem/background/manager/login.jsp");
	}
/**
 * 
 * addUser:(添加管理员). <br/>
 * 
 * @author classA-43 
 * @param request
 * @param response
 * @throws ServletException
 * @throws IOException 
 * @since JDK 1.8
 */
	protected void addUser(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String id = request.getParameter("id");
		String username = request.getParameter("username");
		String gender = request.getParameter("gender");
		String tel = request.getParameter("tel");
		String idcard = request.getParameter("idcard");
		String email = request.getParameter("email");
		String address = request.getParameter("address");
		AdminModel adminModel = new AdminModel();
		AdminService adminService = new AdminService();
		if (null != username && "" != username) {
			adminModel.setAdmin_name(username);
		}
		if (null != gender && "" != gender) {
			adminModel.setAdmin_sex(gender);
		}
		if (null != tel && "" != tel) {
			adminModel.setAdmin_number(tel);
		}
		if (null != idcard && "" != idcard) {
			adminModel.setAdmin_IdNumber(idcard);
		}
		if (null != email && "" != email) {
			adminModel.setAdmin_email(email);
		}
		if (null != address && "" != address) {
			adminModel.setAdmin_address(address);
		}
		int i = 0;
		if(id!=null&&id.equals("")){
			adminModel.setAdmin_pass("123456");
			i = adminService.addUser(adminModel);
		}else{
			adminModel.setId(Integer.parseInt(id));
			i=adminService.updateAdmin(adminModel);
		}
		if (i > 0) {
			request.getRequestDispatcher("/adminServlet").forward(request, response);
		} else {
			request.getRequestDispatcher("background/manager/register.jsp").forward(request, response);
		}
	}

	/**
	 * 
	 * getAdminInfoByID:(根据ID查找管理员信息). <br/>
	 * 
	 * @author classA-43 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException 
	 * @since JDK 1.8
	 */
	protected void getAdminInfoByID(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String id = request.getParameter("id");
		AdminService adminService = new AdminService();
		List<AdminModel> list =adminService.findAllUserById(id);
		if (list != null && list.size() > 0) {
			request.setAttribute("aList", list.get(0));
		}
		request.getRequestDispatcher("background/manager/register.jsp").forward(request, response);
	}
	/**
	 * 
	 * changeAdminPass:(更改密码). <br/>
	 * 
	 * @author classA-43 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException 
	 * @since JDK 1.8
	 */
	
	protected void changeAdminPass(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
	response.setContentType("text/html;charset=utf-8"); 
	String username= (String) request.getSession().getAttribute("username");
	String password= request.getParameter("password");
	AdminService adminService = new AdminService();
	int i = adminService.updatePassword(password, username);
	PrintWriter out = response.getWriter();
     if(i>0){
    	 out.print("更改密码成功");
     }else{
    	 out.print("更改密码失败");
     }
	}

}
