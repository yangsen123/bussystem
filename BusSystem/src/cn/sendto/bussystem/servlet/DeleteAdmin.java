package cn.sendto.bussystem.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.sendto.bussystem.service.AdminService;
import net.sf.json.JSONObject;

@WebServlet("/deleteAdmin")
public class DeleteAdmin extends HttpServlet {


	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String id = request.getParameter("id");
		AdminService adminService = new AdminService();
		int i =0 ;
		if(id!=""&&id!=null){
			i=adminService.deleteUser(id);
		}
		Map< String, String> map = new  HashMap<String, String>();
		PrintWriter out = response.getWriter();
		if(i>0){
			map.put("succ","true");
			out.write(JSONObject.fromObject(map).toString());
		}else{
			map.put("succ","false");
			out.write(JSONObject.fromObject(map).toString());
		}
		out.flush();
		out.close();
	}

}
