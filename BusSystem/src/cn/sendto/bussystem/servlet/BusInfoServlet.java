package cn.sendto.bussystem.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.sendto.bussystem.model.BusModel;
import cn.sendto.bussystem.service.BusService;

@WebServlet("/busInfoServlet")
public class BusInfoServlet extends HttpServlet {


	private static final long serialVersionUID = 9193468603624691291L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String type = request.getParameter("method");
		if (null != type && "" != type) {
			if (type.equals("getInfo")) {
				this.getBusInfo(request, response);
			} else if (type.equals("addInfo")) {
				this.addBusInfo(request, response);
			} else if (type.equals("edi")) {
				this.getBusInfoByID(request, response);
			}
		}
	}

	/**
	 * 
	 * getBusInfo:(得到公交车信息). <br/>
	 * 
	 * @author classA-43
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 * @since JDK 1.8
	 */
	protected void getBusInfo(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		BusService busService = new BusService();
		List<BusModel> list = null;
		list = busService.findBusName();
		if (list != null && list.size() > 0) {
			request.setAttribute("bus", list);
		}
		request.getRequestDispatcher("background/manager/busList.jsp").forward(request, response);
	}

	/**
	 * 
	 * addBusInfo:(��ӹ�������Ϣ). <br/>
	 * 
	 * @author classA-43
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 * @since JDK 1.8
	 */
	protected void addBusInfo(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String id = request.getParameter("id");
		String bus_name = request.getParameter("bus_name");
		String first_station = request.getParameter("first_station");
		String last_station = request.getParameter("last_station");
		String first_bus = request.getParameter("first_bus");
		String last_bus = request.getParameter("last_bus");
		String stations_num = request.getParameter("stations_num");
		String time_interval = request.getParameter("time_interval");
		String line_length = request.getParameter("line_length");
		String company = request.getParameter("company");
		String carfare = request.getParameter("carfare");

		BusService busService = new BusService();

		BusModel busModel = new BusModel();

		if (bus_name != "" && bus_name != null) {
			busModel.setBus_name(bus_name);
		}
		if (first_bus != null && first_bus != "") {
			busModel.setFirst_bus(first_bus);
		}
		if (last_bus != null && last_bus != "") {
			busModel.setLast_bus(last_bus);
		}
		if (first_station != null && first_station != "") {
			busModel.setFirst_station(first_station);
		}
		if (last_station != null && last_station != "") {
			busModel.setLast_station(last_station);
		}
		if (stations_num != null && stations_num != "") {
			busModel.setStations_num(Integer.parseInt(stations_num));
		}

		if (line_length != null && line_length != "") {
			busModel.setLine_length(Double.parseDouble(line_length));
		}
		if (time_interval != null && time_interval != "") {
			busModel.setTime_interval(time_interval);
		}
		if (company != null && company != "") {
			busModel.setCompany(company);
		}
		if (carfare != null && carfare != "") {
			busModel.setCarfare(carfare);
		}
		int i = 0;
		if (id != null && id != "") {
			busModel.setId(Integer.parseInt(id));
			i = busService.updateBusInfo(busModel);
		} else {
			i = busService.addBusInfo(busModel);
		}
		if (id != null && id != "") {
			if (i > 0) {
				request.setAttribute("err", "修改成功");
				request.getRequestDispatcher("/busInfoServlet?method=getInfo").forward(request, response);
			} else {
				request.setAttribute("err", "系统异常");
				request.getRequestDispatcher("/busInfoServlet?method=getInfo").forward(request, response);
			}
		} else {
			if (i > 0) {
				request.setAttribute("bus_name", bus_name);
				request.setAttribute("id", i);
				request.getRequestDispatcher("/BusServlet").forward(request, response);
			} else {
				request.setAttribute("err", "系统异常");
				request.getRequestDispatcher("background/manager/addBusInfo.jsp").forward(request, response);
			}
		}
	}

	protected void getBusInfoByID(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String id = request.getParameter("id");
		BusService busService = new BusService();
		List<BusModel> list = null;
		list = busService.findBusInfoById(id);
		if (list != null && list.size() > 0) {
			request.setAttribute("bus", list.get(0));
		}
		request.getRequestDispatcher("background/manager/addBusInfo.jsp").forward(request, response);
	}

}
