package cn.sendto.bussystem.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.sendto.bussystem.model.BusInfoModel;
import cn.sendto.bussystem.service.BusService;

@WebServlet("/queryBusInfo")
public class QueryBusInfo extends HttpServlet {

	private static final long serialVersionUID = 1L;
	BusService busService = new BusService();
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String type = request.getParameter("method");
		if (null != type && "" != type) {
			if (type.equals("query")) {
				this.queryBusInfoByName(request, response);
			}else if(type.equals("find")){
				this.findBusInfoByStopName(request, response);
			}

		}

	}
/**
 * 
 * queryBusInfoByName:(查找公交车信息根据编号). <br/>
 * 
 * @author classA-43 
 * @param request
 * @param response
 * @throws ServletException
 * @throws IOException 
 * @since JDK 1.8
 */
	protected void queryBusInfoByName(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String busNumber=request.getParameter("busNumber");
		List<BusInfoModel> list = null;
		if(busNumber!=null&&busNumber!=""){
			list = busService.findBusInfoByName(busNumber);
			if(list.size()>0){
				request.setAttribute("bus", list);
			}
			request.getRequestDispatcher("/foreground/busInfo.jsp").forward(request, response);
		}else{
			response.sendRedirect("/BusSystem/foreground/index.jsp");
		}
		
		
	}
	
	/**
	 * 
	 * findBusInfoByStopName:(根据站点查找公交信息). <br/>
	 * 
	 * @author classA-43 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException 
	 * @since JDK 1.8
	 */
	
	
	protected void findBusInfoByStopName(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String station=request.getParameter("station");
		List<BusInfoModel> list = null;
		if(station!=null&&station!=""){
			list = busService.findStopByStopName(station);
			if(list.size()>0){
				request.setAttribute("bus", list);
			}
			request.getRequestDispatcher("/foreground/busInfo.jsp").forward(request, response);
		}else{
			response.sendRedirect("/BusSystem/foreground/index.jsp");
		}
		
		
	}

}
