package cn.sendto.bussystem.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.sendto.bussystem.model.StopModel;
import cn.sendto.bussystem.service.StopService;
@WebServlet("/addStopServlet")
public class AddStopServlet extends HttpServlet {
	StopService StopService=new StopService();

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	this.doPost(request, response);
	}
	@Override 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String stopName=request.getParameter("str");
		String busId=request.getParameter("id");
        StopModel stopModel=new StopModel();
        stopModel.setStop_name(stopName);
        stopModel.setBusId(Integer.parseInt(busId));
        int i=StopService.addStop(stopModel);
        PrintWriter out=response.getWriter();
        if(i>0){
        	out.write("true");
        }else{
        	out.write("false");
        }
        out.flush();
        out.close();
	}

}
