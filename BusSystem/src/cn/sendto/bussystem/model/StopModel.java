package cn.sendto.bussystem.model;

public class StopModel {
	private int stop_no;
	private int busId;
	private String stop_name;

	public int getStop_no() {
		return stop_no;
	}

	public void setStop_no(int stop_no) {
		this.stop_no = stop_no;
	}

	public String getStop_name() {
		return stop_name;
	}

	public void setStop_name(String stop_name) {
		this.stop_name = stop_name;
	}

	public int getBusId() {
		return busId;
	}

	public void setBusId(int busId) {
		this.busId = busId;
	}

}
