package cn.sendto.bussystem.model;
/**
 * 
 * ClassName: BusModel <br/> 
 * Function:公交车实体类（无站点） <br/> 
 * date: 2017年3月22日 下午2:33:47 <br/> 
 * 
 * @author classA-43 
 * @version  
 * @since JDK 1.8
 */
public class BusModel {
	private int id;//id
	private String bus_name;//线路名称
	private String first_station;//始发站
	private String last_station;//终点站
	private String first_bus;//首班车
	private String last_bus;//末班车
	private int stations_num;//站点数目
	private String time_interval;//发车间隔
	private Double line_length;//线路长度
	private String company;//公交公司
	private String  carfare;//全程票价
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBus_name() {
		return bus_name;
	}
	public void setBus_name(String bus_name) {
		this.bus_name = bus_name;
	}
	public String getFirst_station() {
		return first_station;
	}
	public void setFirst_station(String first_station) {
		this.first_station = first_station;
	}
	public String getLast_station() {
		return last_station;
	}
	public void setLast_station(String last_station) {
		this.last_station = last_station;
	}
	public String getFirst_bus() {
		return first_bus;
	}
	public void setFirst_bus(String first_bus) {
		this.first_bus = first_bus;
	}
	public String getLast_bus() {
		return last_bus;
	}
	public void setLast_bus(String last_bus) {
		this.last_bus = last_bus;
	}
	public int getStations_num() {
		return stations_num;
	}
	public void setStations_num(int stations_num) {
		this.stations_num = stations_num;
	}
	public String getTime_interval() {
		return time_interval;
	}
	public void setTime_interval(String time_interval) {
		this.time_interval = time_interval;
	}
	public Double getLine_length() {
		return line_length;
	}
	public void setLine_length(Double line_length) {
		this.line_length = line_length;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getCarfare() {
		return carfare;
	}
	public void setCarfare(String carfare) {
		this.carfare = carfare;
	}
	
}
