package cn.sendto.bussystem.model;

public class AdminModel {
	private int id;
	private String admin_number;
	private String admin_pass;
	private String admin_name;
	private String admin_sex;
	private String admin_IdNumber;
	private String admin_email;
	private String admin_address;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAdmin_number() {
		return admin_number;
	}

	public void setAdmin_number(String admin_number) {
		this.admin_number = admin_number;
	}

	public String getAdmin_pass() {
		return admin_pass;
	}

	public void setAdmin_pass(String admin_pass) {
		this.admin_pass = admin_pass;
	}

	public String getAdmin_name() {
		return admin_name;
	}

	public void setAdmin_name(String admin_name) {
		this.admin_name = admin_name;
	}

	public String getAdmin_sex() {
		return admin_sex;
	}

	public void setAdmin_sex(String admin_sex) {
		this.admin_sex = admin_sex;
	}

	public String getAdmin_IdNumber() {
		return admin_IdNumber;
	}

	public void setAdmin_IdNumber(String admin_IdNumber) {
		this.admin_IdNumber = admin_IdNumber;
	}

	public String getAdmin_email() {
		return admin_email;
	}

	public void setAdmin_email(String admin_email) {
		this.admin_email = admin_email;
	}

	public String getAdmin_address() {
		return admin_address;
	}

	public void setAdmin_address(String admin_address) {
		this.admin_address = admin_address;
	}

}
