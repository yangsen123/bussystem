package cn.sendto.bussystem.service;

import java.util.List;

import cn.sendto.bussystem.dao.AdminDao;
import cn.sendto.bussystem.model.AdminModel;

public class AdminService {
	AdminDao adminDao = new AdminDao();

	/**
	 * 
	 * findAllUser:(). <br/>
	 * 
	 * @author classA-43
	 * @return
	 * @since JDK 1.8
	 */
	public List<AdminModel> findAllUser() {
		List<AdminModel> list = null;
		try {
			adminDao.begins();
			list = adminDao.findAllUser();
			adminDao.commit();
		} catch (Exception e) {
			try {
				adminDao.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			adminDao.close();
		}

		return list;

	}
/**
 * 
 * login:. <br/>
 * 
 * @author classA-43 
 * @param username
 * @param password
 * @return 
 * @since JDK 1.8
 */
	public List<AdminModel> login(String username, String password) {
		List<AdminModel> list = null;
		try {
			adminDao.begins();
			list = adminDao.login(username, password);
			adminDao.commit();
		} catch (Exception e) {
			try {
				adminDao.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			adminDao.close();
		}
		return list;
	}
	/**
	 * 
	 * addUser:. <br/>
	 * 
	 * @author classA-43 
	 * @param adminModel
	 * @return 
	 * @since JDK 1.8
	 */
	public int addUser(AdminModel adminModel) {
		int i = 0 ;
		try {
			adminDao.begins();
		i=	adminDao.addUser(adminModel);
			adminDao.commit();
		} catch (Exception e) {
			try {
				adminDao.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally{
			adminDao.close();
		}
		return i;
	}
	/**
	 * 
	 * deleteUser:. <br/>
	 * 
	 * @author classA-43 
	 * @param id
	 * @return 
	 * @since JDK 1.8
	 */
	public int deleteUser(String id) {
		int i = 0 ;
		try {
			adminDao.begins();
		i=	adminDao.deleteUser(id);
			adminDao.commit();
		} catch (Exception e) {
			try {
				adminDao.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally{
			adminDao.close();
		}
		return i;
	}
	/**
	 * 
	 * updateAdmin:. <br/>
	 * 
	 * @author classA-43 
	 * @param id
	 * @return 
	 * @since JDK 1.8
	 */
	public int updateAdmin(AdminModel adminModel) {
		int i = 0 ;
		try {
			adminDao.begins();
		i=	adminDao.updateAdmin( adminModel);
			adminDao.commit();
		} catch (Exception e) {
			try {
				adminDao.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally{
			adminDao.close();
		}
		return i;
	}
	/**
	 * 
	 * findAllUserById:(). <br/>
	 * 
	 * @author classA-43 
	 * @param id
	 * @return 
	 * @since JDK 1.8
	 */
	public List<AdminModel> findAllUserById(String id) {
		List<AdminModel> list = null;
		try {
			adminDao.begins();
			list = adminDao.findAllUserById(id);
			adminDao.commit();
		} catch (Exception e) {
			try {
				adminDao.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			adminDao.close();
		}

		return list;

	}
	/**
	 * 
	 * updatePassword:(). <br/>
	 * 
	 * @author classA-43 
	 * @param password
	 * @param id
	 * @return 
	 * @since JDK 1.8
	 */
	public int updatePassword(String password, String username) {
		int i = 0 ;
		try {
			adminDao.begins();
		i=	adminDao.updatePassword(password, username);
			adminDao.commit();
		} catch (Exception e) {
			try {
				adminDao.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally{
			adminDao.close();
		}
		return i;
	}
	
	

}
