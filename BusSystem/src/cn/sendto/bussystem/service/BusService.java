package cn.sendto.bussystem.service;

import java.util.List;

import cn.sendto.bussystem.dao.BusDao;
import cn.sendto.bussystem.dao.StopDao;
import cn.sendto.bussystem.model.BusInfoModel;
import cn.sendto.bussystem.model.BusModel;

public class BusService {
	BusDao busDao = new BusDao();

	/**
	 * 
	 * findBusInfoById <br/>
	 * 
	 * @author classA-43 
	 * @param id
	 * @return 
	 * @since JDK 1.8
	 */
	
	public List<BusModel> findBusInfoById(String id) {
		List<BusModel> list = null;
		try {
			list = busDao.findBusInfoById(id);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			busDao.close();
		}
		return list;
	}
	

		public List<BusModel> findBusName() {
			List<BusModel> list = null;
			try {
				list = busDao.findBusName();
			} catch (Exception e) {
				e.printStackTrace();
			}finally{
				busDao.close();
			}
			return list;
		}
	/**
	 * 
	 * addBusInfo:(). <br/>
	 * 
	 * @author classA-43 
	 * @param BusModel
	 * @return 
	 * @since JDK 1.8
	 */
	public int addBusInfo(BusModel BusModel) {
		int i = 0;
		try {
			busDao.begins();
			i = busDao.addBusInfo(BusModel);
			busDao.commit();
		} catch (Exception e) {
			try {
				busDao.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally{
			busDao.close();
		}
		return i;

	}
	/**
	 * 
	 * deleteBusInfo:(). <br/>
	 * 
	 * @author classA-43 
	 * @param id
	 * @return 
	 * @since JDK 1.8
	 */
	public int deleteBusInfo(String id){
		StopDao stopDao =new StopDao();
		int i = 0;
		try {
			busDao.begins();
			stopDao.deleteStop(id);
			i = busDao.deleteBusInfo(id);
			busDao.commit();
		} catch (Exception e) {
			try {
				busDao.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally{
			busDao.close();
		}
		return i;
	}
	/**
	 * 
	 * updateBusInfo:(). <br/>
	 * 
	 * @author classA-43 
	 * @param busInfo
	 * @return 
	 * @since JDK 1.8
	 */
	public int updateBusInfo(BusModel busInfo){
		int i = 0;
		try {
			busDao.begins();
			i = busDao.updateBusInfo(busInfo);
			busDao.commit();
		} catch (Exception e) {
			try {
				busDao.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally{
			busDao.close();
		}
		return i;
	}
	/**
	 * 
	 * findBusInfoByName. <br/>
	 * 
	 * @author classA-43 
	 * @param busName
	 * @return 
	 * @since JDK 1.8
	 */
	public List<BusInfoModel> findBusInfoByName(String busName) {
		List<BusInfoModel> list = null;
		try {
			list = busDao.findBusInfoByName(busName);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			busDao.close();
		}
		return list;
	}
	
	/**
	 * 
	 * findStopByStopName:. <br/>
	 * 
	 * @author classA-43 
	 * @param stopName
	 * @return 
	 * @since JDK 1.8
	 */
	public List<BusInfoModel> findStopByStopName(String stopName){
		List<BusInfoModel> list = null;
		try {
			list = busDao.findStopByStopName(stopName);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			busDao.close();
		}
		return list;
		
	}
	
}
