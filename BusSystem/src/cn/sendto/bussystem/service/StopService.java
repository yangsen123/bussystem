package cn.sendto.bussystem.service;

import java.util.List;

import cn.sendto.bussystem.dao.StopDao;
import cn.sendto.bussystem.model.StopModel;

public class StopService {
	StopDao stopDao = new StopDao();

	public List<StopModel> findAllStop(String busId) {
		List<StopModel> list = null;
		try {
			list = stopDao.findAllStop(busId);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			stopDao.close();
		}
		return list;
	}
	/**
	 * 
	 * addStop. <br/>
	 * 
	 * @author classA-43 
	 * @param stop
	 * @return 
	 * @since JDK 1.8
	 */
	public int addStop(StopModel stop){
		int i = 0;
		try {
			stopDao.begins();
			i = stopDao.addStop(stop);
			stopDao.commit();
		} catch (Exception e) {
			try {
				stopDao.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			stopDao.close();
		}
		return i;
	}
	/**
	 * 
	 * findStopById. <br/>
	 * 
	 * @author classA-43 
	 * @param busId
	 * @return 
	 * @since JDK 1.8
	 */
	public List<StopModel> findStopById(int busId){
		List<StopModel> list = null;
		try {
			list = stopDao.findStopById(busId);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			stopDao.close();
		}
		return list;
	}
	
	

}
