$(function(){
	$(".Change1").blur(function(){
		checkLine();
	});
	
	$(".Change2").blur(function(){
		checkFirst();
	});	
	$(".Change3").blur(function(){
		checkLast();
	});
	$(".Change4").blur(function(){
		checkFirstBus();
	});
	$(".Change5").blur(function(){
		 checkLastBus();
	});
	$(".Change6").blur(function(){
		checkStationsNum();
	});
	$(".Change7").blur(function(){
		checkTimeInterval();
	});
	$(".Change8").blur(function(){
		checkCrfare();
	});
	$(".Change9").blur(function(){
		checkLineLength();
	});
	$(".Change0").blur(function(){
		 checkCompany();
	});
	
	$("#addbut").click(function(){
		if(checkLine()&&checkFirst()&&checkLast()&&checkFirstBus()&& checkLastBus()&&checkLast()&& checkStationsNum()&&checkTimeInterval()&&checkCrfare()&&checkLineLength()&& checkCompany()){
			$("#from").submit();
		}
	});
	
	$("#updbut").click(function(){
		if(checkLine()&&checkFirst()&&checkLast()&&checkFirstBus()&& checkLastBus()&&checkLast()&& checkStationsNum()&&checkTimeInterval()&&checkCrfare()&&checkLineLength()&& checkCompany()){
			$("#from").submit();
		}
	});
	
	$("#res").click(function(){
		$(".errorDiv1").text(" ");
		$(".errorDiv2").text(" ");
		$(".errorDiv3").text(" ");
		$(".errorDiv4").text(" ");
		$(".errorDiv5").text(" ");
		$(".errorDiv6").text(" ");
		$(".errorDiv7").text(" ");
		$(".errorDiv8").text(" ");
		$(".errorDiv9").text(" ");
		$(".errorDiv0").text(" ");
	});
});


function checkLine(){
	var flag=false;
	var bus_name = $("#bus_name").val();
	if($.trim(bus_name)==""){
		$("#bus_name").focus();
		$(".errorDiv1").text("*请填写线路编号");
	}else{
		flag=true;
		$(".errorDiv1").html("<img src='/BusSystem/background/images/right.png' width='20' height='20' />");
	}
	return flag;
}
function checkFirst(){
		var flag=false;
		var first_station = $("#first_station").val();
		if($.trim(first_station)==""){
			 $("#first_station").focus();
			$(".errorDiv2").text("*请填写起点站");
		}else{
			flag=true;
			$(".errorDiv2").html("<img src='/BusSystem/background/images/right.png' width='20' height='20' />");
		}
		return flag;
}


function checkLast(){
	var flag=false;
	var last_station = $("#last_station").val();
	if($.trim(last_station)==""){
		 $("#last_station").focus();
		$(".errorDiv3").text("*请填写终点站");
	}else{
		flag=true;
		$(".errorDiv3").html("<img src='/BusSystem/background/images/right.png' width='20' height='20' />");
	}
	return flag;
}

function checkFirstBus(){
	var flag=false;
	var first_bus = $("#first_bus").val();
	if($.trim(first_bus)==""){
		 $("#first_bus").focus();
		$(".errorDiv4").text("*请填写首班车");
	}else{
		flag=true;
		$(".errorDiv4").html("<img src='/BusSystem/background/images/right.png' width='20' height='20' />");
	}
	return flag;
}
function checkLastBus(){
	var flag=false;
	var last_bus = $("#last_bus").val();
	if($.trim(last_bus)==""){
		 $("#last_bus").focus();
		$(".errorDiv5").text("*请填写末班车");
	}else{
		flag=true;
		$(".errorDiv5").html("<img src='/BusSystem/background/images/right.png' width='20' height='20' />");
	}
	return flag;
}
function checkStationsNum(){
	var flag=false;
	var stations_num = $("#stations_num").val();
	if($.trim(stations_num)==""){
		 $("#stations_num").focus();
		$(".errorDiv6").text("*请填写站点数");
	}else{
		if(isNaN(stations_num)){
			$("#stations_num").val("");
			$("#stations_num").focus();
			$(".errorDiv6").text("*站点数应为数字");
		}else{
				flag=true;
				$(".errorDiv6").html("<img src='/BusSystem/background/images/right.png' width='20' height='20' />");
			}
		}
	return flag;
}
function checkTimeInterval(){
	var flag=false;
	var time_interval = $("#time_interval").val();
	if($.trim(time_interval)==""){
		$("#time_interval").focus();
		$(".errorDiv7").text("*请填写发车间隔");
	}else{
		flag=true;
		$(".errorDiv7").html("<img src='/BusSystem/background/images/right.png' width='20' height='20' />");
	}
	return flag;
}
function checkCrfare(){
	var flag=false;
   var carfare = $("#carfare").val();
if($.trim(carfare)==""){
	$("#carfare").focus();
	$(".errorDiv8").text("*请填写全程票价");
}else{
	flag=true;
	$(".errorDiv8").html("<img src='/BusSystem/background/images/right.png' width='20' height='20' />");
}
	return flag;
}
function checkLineLength(){
	var flag=false;
	var line_length = $("#line_length").val();
	if($.trim(line_length)==""){
		$("#line_length").focus();
		$(".errorDiv9").text("*请填写线路长度");
	}else{
		if(isNaN(line_length)){
			$("#line_length").val("");
			$("#line_length").focus();
			$(".errorDiv9").text("*线路长度应为数字");
		}else{
			flag=true;
			$(".errorDiv9").html("<img src='/BusSystem/background/images/right.png' width='20' height='20' />");
		}
	}
	return flag;
}
function checkCompany(){
	var flag=false;
	var company = $("#company").val();
	if($.trim(company)==""){
		$("#company").focus();
		$(".errorDiv0").text("*请填写所属公司");
	}else{
		flag=true;
		$(".errorDiv0").html("<img src='/BusSystem/background/images/right.png' width='20' height='20' />");
	}
	return flag;
}