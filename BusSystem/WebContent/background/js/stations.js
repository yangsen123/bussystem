$(function(){
	
	var bus_name=$("#bus_name").val();
	if(bus_name!=""&&bus_name!=null){
		line($("#bus_id").val());
		$("#chooseLine").val($("#bus_id").val());
	}
	
	$(document).on("change","#chooseLine",function(){
		line($("#chooseLine").val());
	});
	
	$(document).on("click","#addStop",function(){
		var num="";
		$.ajax({
			type:"post",
			url:"/BusSystem/BusServlet",
			async:false,
			data:{id:$("#chooseLine").val()},
			success:function(data){
				if(data!=null&&data!=""){
					num=data;
				}
			}
		});
		var str="";
		for (var i = 1; i <= num; i++) {
			if($("#station"+i).val()==""){
				alert("请输入第"+i+"个站点");
				return;
			}
			str+=$("#station"+i).val()+",";
		}
		$.ajax({
			type:"post",
			url:"/BusSystem/addStopServlet",
			async:false,
			data:{
				str:str,
				id:$("#bus_id").val()
			},
			success:function(data){
			if($.trim(data)=="true"){
				alert("添加成功");
			}
			window.location.reload();
			}
		});
	});
});
function  line(id){
	if(id==0){
		$("#station_num").html("");
		return;
	}
	$.ajax({
		type:"post",
		url:"/BusSystem/BusServlet",
		async:true,
		data:{id:id},
		success:function(data){
			if(data!=null&&data!=""){
				if(data=="0"){
					$("#station_num").html("此站点已经添加，无需继续添加");
				}else{
					var str="";
					for (var i=0;i<data;i++) {
						str +=('<div style="height:25px;"><input type="text" id="station'+(i+1)+'" name="station'+(i+1)+'" placeholder="请输入第'+(i+1)+'个站点"  value=""/></div>');
					}
					str+='<div style="height:10px;"></div><input type="button" id="addStop" value="添加"/>';
					$("#station_num").html(str);
				}
				$("#bus_id").val(id);
			}
		}
	});
}