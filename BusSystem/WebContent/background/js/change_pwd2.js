function checkPass(){
		var oldPwd = $("#oldPwd").val();
		var password= $("#password").val();
		var newPwd= $("#newPwd").val();
		var sureNewPwd= $("#sureNewPwd").val();
		if(oldPwd==""){
			$(".errdiv").text("请输入原始密码");
			return false;
		}else if(oldPwd.length<6){
			$(".errdiv").text("原始密码长度不能小于6");
			return false;
		}
		if(newPwd==""){
			$(".errdiv").text("请输入新密码");
			return false;
		}else if(newPwd.length<6){
			$(".errdiv").text("新密码长度不能小于6");
			return false;
		}
		if(sureNewPwd==""){
			$(".errdiv").text("请输入确认密码");
			return false;
		}
		if(newPwd==sureNewPwd){
			if(password==oldPwd){
				$.ajax({
					type:"post",
            		url:"/BusSystem/logServlet?method=changePass",
            		data:{password:newPwd},
            		success:function (data){
            			if(data!=null&&""!=data){
            				$("#oldPwd").val("");
            				$("#newPwd").val("");
            				$("#sureNewPwd").val("");
            				if($.trim(data)=="更改密码成功"){
            					window.top.location.href="/BusSystem/logServlet?method=out"
            				}else{
            					$(".errdiv").text(data);
            				}
            			}else{
            				$(".errdiv").text("系统繁忙,请重试!!");
            			}
            		}
				});
			}else{
				$(".errdiv").text("原始密码不正确");
			}
		}else{
			$(".errdiv").text("两次密码输入不一致")
		}
	}