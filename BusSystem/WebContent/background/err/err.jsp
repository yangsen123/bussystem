<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isErrorPage="true"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>出错页面</title>
<link type="text/css" rel="stylesheet" href="../css/err.css" />
</head>
<body>
<div class="div">
<img alt="对不起，出错了" src="../images/err.jpg" class="img"/>
</div>
</body>
</html>