<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
		<form action="/BusSystem/logServlet?method=register" method="post">
		<input type="hidden" name="id" value="${aList.id }"  />
			<table width="400px" height="500px" style="margin-left: 400px">
				<tr>
					<td colspan="2" align="center" style="font-size: 25px;font-weight: bold;">添加管理员信息</td>
				</tr>
				<tr>
					<td align="center" colspan="2" class="red" height="20px">
						
					</td>
				</tr>
				<tr>
					<td height="40px" align="right">姓&nbsp;&nbsp;名：</td>
					<td align="left"><input type="text" required="required" placeholder="请输入姓名" name="username"   value="${aList.admin_name }" /> </td>
				</tr>
				<tr>
					<td height="40px" align="right">性&nbsp;&nbsp;别：</td>
					<td align="left"><input type="radio" name="gender" <c:choose>
					<c:when test="${aList.admin_sex eq '男' }">checked="checked"</c:when>
					<c:when test="${empty aList }">checked="checked"</c:when>
				</c:choose> value="男"/>男
					<input type="radio" name="gender"<c:if test="${aList.admin_sex eq '女' }">checked="checked"</c:if> value="女" />女</td>
				</tr>
				<tr>
					<td height="40px" align="right">联系电话：</td>
					<td align="left"><input type="text" required="required" placeholder="请输入电话" maxlength="11" name="tel" pattern="1[3|5|8|7][0-9]{9}" value="${aList.admin_number }" /></td>
				</tr>
				<tr>
					<td height="40px" align="right">身份证号：</td>
					<td align="left"><input type="text" required="required" placeholder="请输入身份证号" name="idcard" pattern="[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)" value="${aList.admin_IdNumber }" /> </td>
				</tr>
				<tr>
					<td height="40px" align="right">邮&nbsp;&nbsp;箱：</td>
					<td align="left"><input type="text" required="required" placeholder="请输入邮箱" name="email"  value="${aList.admin_email }" /> </td>
				</tr>
				<tr>
					<td height="40px" align="right">住&nbsp;&nbsp;址：</td>
					<td align="left"><input type="text" required="required" placeholder="请输入住址" name="address"  value="${aList.admin_address }" /> </td>
				</tr>
				<tr><td colspan="2" align="center" style="font-size: 12px;color: gray;">默认密码为123456</td> </tr>
				<tr>
				
				<c:choose>
					<c:when test="${empty aList }">
					<td colspan="2" align="center"><input type="submit" name="submit" value="确认添加" /> </td>
					</c:when>
					<c:otherwise>
					<td colspan="2" align="center"><input type="submit" name="submit" value="确认修改" /> </td>
					</c:otherwise>
				</c:choose>
				</tr>
			</table>
		</form>
</body>
</html>