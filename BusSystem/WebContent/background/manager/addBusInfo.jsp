<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<c:set var="path" value="${pageContext.request.contextPath}"/>
<c:if test="${empty username }">
	<c:redirect url="/background/manager/login.jsp"/>
</c:if>
 <!DOCTYPE>
<html>
	<head>
		<meta charset="UTF-8">
		<title>添加线路</title>
		<script type="text/javascript" src="/BusSystem/background/js/jquery-1.12.3.min.js"></script>
		<script type="text/javascript" src="/BusSystem/background/js/addBusInfo.js"></script>
		<style type="text/css">
.div_main {
	margin-left: 400px;

	width: 520px;
}
#errorDiv{
font-size: 12px;
color: red;
}
table{
margin-left: 50px;
}
.input{
width: 200px;
height:25px;
border-radius:3px;
border:1px solid gray;
}
</style>
	</head>
	<body>
	<div class="div_main" >
	<form action="/BusSystem/busInfoServlet" id="from" method="post">
	<input type="hidden" name="method" value="addInfo"/>
	<input type="hidden" name="id" value="${bus.id }"/>
	
		<table  cellspacing="20%" cellpadding="13px">
			<tr>
				<th>线路编号:</th>
				<td><input  class="Change1 input" type="text" name="bus_name" id="bus_name" value="${bus.bus_name }" placeholder="例如：'161路'" required="required"/></td>
			<td><div class="errorDiv1"  id="errorDiv"></div></td>
			</tr>
			<tr>
				<th>起点站:</th>
				<td><input type="text"   class="Change2 input" name="first_station" id="first_station" value="${bus.first_station }" placeholder="例如：'大唐芙蓉园南门'" required="required" /></td>
			<td><div class="errorDiv2"  id="errorDiv"></div></td>
			</tr>
			<tr>
				<th>终点站:</th>
				<td><input type="text"   class="Change3 input" name="last_station" id="last_station" value="${bus.last_station }" placeholder="例如：'电视塔'" required="required"/></td>
			<td><div class="errorDiv3"  id="errorDiv"></div></td>
			</tr>
			<tr>
				<th>首班车:</th>
				<td><input type="time"   class="Change4 input" name="first_bus" id="first_bus" value="${bus.first_bus }" placeholder="例如：'06:30'" required="required"/></td>
			<td><div class="errorDiv4"  id="errorDiv"></div></td>
			</tr>
			<tr>
				<th>末班车:</th>
				<td><input type="time"   class="Change5 input" name="last_bus" id="last_bus" value="${bus.last_bus }" placeholder="例如：'19:30'" required="required"/></td>
			<td><div class="errorDiv5"  id="errorDiv"></div></td>
			</tr>
			<tr>
				<th>站点数目:</th>
				<td><input type="text"   class="Change6 input" name="stations_num" id="stations_num" value="${bus.stations_num }" placeholder="例如：'11'，默认单位:站" required="required"/></td>
			<td><div class="errorDiv6"  id="errorDiv"></div></td>
			</tr>
			<tr>
				<th>发车间隔:</th>
				<td><input type="text"   class="Change7 input" name="time_interval" id="time_interval" value="${bus.time_interval }" placeholder="例如：'5分钟'" required="required"/></td>
			<td><div class="errorDiv7"  id="errorDiv"></div></td>
			</tr>
			<tr>
				<th>票&nbsp;价:</th>
				<td><input type="text"  class="Change8 input"  name="carfare" id="carfare" value="${bus.carfare }" placeholder="例如：'2元'" required="required"/></td>
			<td><div class="errorDiv8"  id="errorDiv"></div></td>
			</tr>
			<tr>
				<th>线路长度:</th>
				<td><input type="text"  class="Change9 input"  name="line_length" id="line_length" value="${bus.line_length }" placeholder="例如：'6.8'，默认单位:公里" required="required"/></td>
			<td><div class="errorDiv9"  id="errorDiv"></div></td>
			</tr>
			<tr>
				<th>所属公司:</th>
				<td><input type="text"  class="Change0 input"  name="company" id="company" value="${bus.company }" placeholder="例如：'西安市公共交通总公司第五公司一车队'" required="required"/></td>
			<td><div class="errorDiv0"  id="errorDiv"></div></td>
			</tr>
			<tr> 
			<td></td>
			<c:choose>
			<c:when test="${empty bus }">
			<td><input type="button" id="addbut" value="添加"/>&nbsp;&nbsp;&nbsp;<input type="reset" id="res" value="重置"/></td>
			</c:when>
			<c:otherwise>
			<td><input type="button" id="updbut" value="修改"/>&nbsp;&nbsp;&nbsp;<input type="reset" id="res" value="重置"/></td>
			</c:otherwise>
			</c:choose>
			
			</tr>
		</table>
		</form>
		</div>
	</body>

</html> 
