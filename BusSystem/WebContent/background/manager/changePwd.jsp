<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<c:set var="path" value="${pageContext.request.contextPath}"/>
<!DOCTYPE>
<head>
    <title>修改密码</title>
	<meta charset="UTF-8"/>
    <link rel="stylesheet" href="${path}/background/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="${path}/background/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="${path}/background/font-awesome-4.7.0/css/font-awesome.min.css"/>
    <script type="text/javascript" src="${path}/background/js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="${path}/background/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${path}/background/js/change_pwd2.js"> </script>
</head>
<body>
<script type="text/javascript" src="${path}/background/js/jquery.hoverIntent.js"></script>
    <div class="main">
        <div class="container">
            <div class="rc">
                <div class="order-header">
                    <h5>我的账户&nbsp;/&nbsp;修改密码</h5>
                    <form id="form1" method="post" action="#">
                    <div class="errdiv"></div>
                        <div class="form-group row">
                        <input type="hidden" id="password" value="${password }"/>
                            <label class="col-xs-3 text-right m-pt5">
                                原始密码&nbsp;
                            </label>
                            <div class="col-xs-5">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class=" fa fa-lock"></span></span>
                                    <input type="password" id="oldPwd" name="oldPwd" class="form-control" maxlength="30" placeholder="请输入原始密码"/>
                                </div>
                            </div>
                            <div class="col-xs-4"></div>
                        </div>

                        <div class="form-group row">
                            <label class="col-xs-3 text-right m-pt5">
                                新密码&nbsp;
                            </label>
                            <div class="col-xs-5">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-lock"></span></span>
                                    <input type="password" id="newPwd" name="newPwd" class="form-control" maxlength="30" placeholder="请输入新密码"/>
                                </div>
                            </div>
                            <div class="col-xs-4"></div>
                        </div>

                        <div class="form-group row">
                            <label class="col-xs-3 text-right m-pt5">
                                新密码确认&nbsp;
                            </label>
                            <div class="col-xs-5">
                                <div class="input-group">
                                    <span class="input-group-addon"> <span class="fa fa-lock"></span></span>
                                    <input type="password" id="sureNewPwd" name="sureNewPwd" class="form-control" maxlength="30" placeholder="请再次输入新密码"/>
                                </div>
                            </div>
                            <div class="col-xs-4"></div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-3"></div>
                            <div class="col-xs-5">
                                <button type="button" class="btn btn-success form-control btnChangePwd" onclick="checkPass()">修改密码</button>
                            </div>
                            <div class="col-xs-4"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</body>
</html>
