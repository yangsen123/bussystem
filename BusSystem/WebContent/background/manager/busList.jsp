<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	  <%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
   <c:set var="path" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>公交信息</title>
 <link rel="stylesheet" type="text/css" href="${path }/background/css/busList.css"/>
 <script type="text/javascript"  src="${path }/background/js/jquery-1.9.1.min.js"></script>
 <script type="text/javascript" src="${path }/background/js/deleteBusInfo.js"></script>
</head>
<body>
	<ul class="ul">
			<li class="infoliwidth">线路编号</li>
			<li class="infoliwidth">起点站</li>
			<li class="infoliwidth">终点站</li>
			<li class="infoliwidth">首班车</li>
			<li class="infoliwidth">末班车</li>
			<li class="infoliwidth">站点数</li>
			<li class="infoliwidth">发车间隔(分钟)</li>
			<li class="infoliwidth">全程票价(元)</li>
			<li class="infoliwidth">线路长度(公里)</li>
			<li class="infoliwidth">所属公司</li>
			<li class="infoliwidth">查看站点</li>
			<li class="infoliwidth">编辑</li>
			<li class="infoliwidth">删除</li>
	</ul>
	
	<c:forEach var="bus" items="${bus}">
	<ul>
	<li class="infoliwidth" title="${bus.bus_name }" >${bus.bus_name }</li>
	<li class="infoliwidth" title="${bus.first_station }">${bus.first_station }</li>
	<li class="infoliwidth" title="${bus.last_station }">${bus.last_station }</li>
	<li class="infoliwidth" title="${bus.first_bus }">${bus.first_bus }</li>
	<li class="infoliwidth" title="${bus.last_bus }">${bus.last_bus }</li>
	<li class="infoliwidth" title="${bus.stations_num }">${bus.stations_num }</li>
	<li class="infoliwidth" title="${bus.time_interval }">${bus.time_interval }</li>
	<li class="infoliwidth" title="${bus.carfare }">${bus.carfare }</li>
	<li class="infoliwidth" title="${bus.line_length }">${bus.line_length }</li>
	<li class="infoliwidth" title="${bus.company }">${bus.company }</li>
	<li class="infoliwidth"><a href="/BusSystem/stopServlet?id=${bus.id }"><img  src="${path }/background/images/111.gif" height="20px"></a></li>
	<li class="infoliwidth"><a href="/BusSystem/busInfoServlet?method=edi&id=${bus.id }" ><img	src="${path }/background/images/editer.jpg" name="" /></a></li>
	<li class="infoliwidth"><a href="JavaScript:void(0)" onclick="deleteBusInfo(${bus.id })"><img src="${path }/background/images/delimg.jpg" name="" /></a></li>
	</ul>
</c:forEach>	
</body>
</html>
