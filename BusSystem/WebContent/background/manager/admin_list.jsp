<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	  <%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
   <c:set var="path" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>管理员信息</title>
 <link rel="stylesheet" type="text/css" href="${path }/background/css/busList.css"/>
 <script type="text/javascript" src="${path}/background/js/jquery-1.12.3.min.js"></script>
  <script type="text/javascript" src="${path}/background/js/deleteAdmin.js"></script>
</head>
<body>
	<ul class="ul">
			<li class="liwidth">姓名</li>
			<li class="liwidth">电话</li>
			<li class="liwidth">性别</li>
			<li class="liwidth">身份证号</li>
			<li class="liwidth">Email</li>
			<li class="liwidth">家庭住址</li>
			<li class="liwidth">编辑</li>
			<li class="liwidth">删除</li>
	</ul>
	<c:forEach var="admin" items="${admin}" varStatus="index">
	<ul class="ul1">
	<li class="liwidth" title="${admin.admin_name }">${admin.admin_name }</li>
	<li class="liwidth" title="${admin.admin_number }">${admin.admin_number }</li>
	<li class="liwidth" title="${admin.admin_sex }">${admin.admin_sex }</li>
	<li class="liwidth" title="${admin.admin_IdNumber }">${admin.admin_IdNumber }</li>
	<li class="liwidth" title="${admin.admin_email }">${admin.admin_email }</li>
	<li class="liwidth" title="${admin.admin_address }">${admin.admin_address }</li>
	<li class="liwidth"><c:choose >
		<c:when test="${username eq admin.admin_number}"></c:when>
		<c:otherwise><a href="/BusSystem/logServlet?method=getAdminInfo&id=${admin.id}" ><img	src="${path }/background/images/editer.jpg" name="" /></a></c:otherwise>
	</c:choose></li>
	<li class="liwidth"><c:choose >
		<c:when test="${username eq admin.admin_number}"></c:when>
		<c:otherwise><a id="userid${admin.id}" onclick="deleteAdmin(${admin.id})"><img src="${path }/background/images/delimg.jpg" name="" /></a></c:otherwise>
	</c:choose></li>

	</ul>
</c:forEach>	
</body>
</html>
