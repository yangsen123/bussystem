<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
   <c:set var="path" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>添加站点</title>
<script type="text/javascript"  src="${path }/background/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="${path }/background/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${path }/background/js/stations.js"></script>
<style type="text/css">
.div_select{
text-align:center;
margin-left: 400px;
background-color: #ebebeb;
width: 500px;
min-height: 500px;
}
#chooseLine{
width:171px;
margin-top: 20px;
}
</style>

</head>
<body>
<div class="div_select">
<input type="hidden"  id="bus_name" value="${bus_name}"/>
<input type="hidden"  id="bus_id" value="${id}"/>
<select id="chooseLine" >
<option value="0">--请选择线路--</option>
<c:forEach var="list" items="${list}">
<option value="${list.id }">${list.bus_name }</option>
</c:forEach>
</select>

<div id="station_num">
</div>

</div>
</body>
</html>