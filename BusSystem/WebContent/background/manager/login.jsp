<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<c:set var="path" value="${pageContext.request.contextPath}"/>
<!DOCTYPE ">
<html>
<head>
    <title>公交车查询系统</title>
    <meta name="renderer" content="webkit"/>

    <link rel="stylesheet" href="../css/bootstrap.min.css"/>
  <link rel="stylesheet" href="../css/font-awesome.min.css"/>
     <link rel="stylesheet" href="../css/style.css"/>

   <link rel="stylesheet" type="text/css" href="../css/common.css"/>
    <link rel="stylesheet" type="text/css" href="../css/login.css"/>

    <script type="text/javascript" src="../js/jquery-1.12.3.min.js"></script>
     <script type="text/javascript" src="../js/login.js"></script>
     <script type="text/javascript" src="../js/common.js"></script>
</head>
<body>
<!-- 顶部 S -->
<div class="top">
	<div class="container">
		<div class="lc">
		 </div>
		 <div class="rc">
				您好，请<a href="#" style="margin:0;">登录</a>&nbsp;|
				<a href="${path }/foreground/index.jsp">公交查询</a>&nbsp;|
			    <a href="">系统帮助</a>
			
		</div>
	</div>
</div>
<!-- 顶部 E -->

<!-- 导航条 S -->
<div class="header">
	<div class="container">
		<div class="pull-left text-left">
			<a href="${path }/foreground/index.jsp"><img src="../images/logo2.png" width="165px" border="0" style="height: 51px;"/></a>
		</div>
	</div>
</div>
<!-- 导航条 E -->


    <div class="main green-bg">
        <div class="container row">
            <div class="col-sm-6 pull-left" style="padding-top: 60px;">
                <img src="../images/login_new.png"/>
            </div>

            <div class="col-sm-6 pull-right">
                <div class="login-box">
                    <div class="login-title">账户登录</div>
                    <div class="login-content">
                        <form action="/BusSystem/logServlet?method=in"  method="post"  id="loginForm" >
                        <input  type="hidden" value="login" name="method"/>
                            <div class="form-group" style="margin-bottom: 0;">
                                <div class="col-xs-12 errorTip">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"> <img src="../images/23.gif" /></span>
                                        <input type="text" id="username" name="username" class="form-control input-lg" value="" placeholder="请输入手机号" maxlength="11"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"> <img src="../images/24.gif" /></span>
                                        <input type="password" id="password" name="password" class="form-control input-lg" value="" placeholder="请输入登录密码" maxlength="20"/>
                                    </div>
                                </div>
                            </div>
                               <div class="form-group">
                        <div class="col-xs-7">
                            <div class="input-group">
                                <span class="input-group-addon"> <img src="../images/24.gif" /></span>
                                <input type="text" id="vlCodeInp"  name="vlCodeInp" class="form-control input-lg" maxlength="6" placeholder="图片验证码" onblur="isRightCode();"/>
                            </div>
                        </div>
                        <div class="col-xs-5" style="padding-left:0; padding-right: 0;">
                            <img id="vlCodeImg" src="${path}/imgUtil" width="165" height="45" style="cursor: pointer;" onclick="changeImg();"/>
                        </div>
                    </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <button type="button" class="col-xs-12 btn btn-primary btn-lg loginBtn">立即登录</button>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-6 link text-left">
                                    <p class="remove-margin"><a href="#">忘记密码?</a></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="space"></div>
    <div class="text-center">
       西安翻译学院工程院电子系计算机科学与技术401班毕业设计    |    yangsen 版权所有
    </div>
</body>
</html>
