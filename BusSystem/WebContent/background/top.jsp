<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
     <c:set var="path" value="${pageContext.request.contextPath}"/>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>头部页面</title>
	<link rel="stylesheet" type="text/css" href="css/top.css" />
	</head>
	<body height="120px">
<div id="div_1">
    		<table width="100%" >
    			<tr>
    				<td rowspan="2" width="300" align="center"><img src="images/logo.gif" height="65"></td>
    				<td rowspan="2"  width="60%" class="titie"  align="center">公交车查询系统</td>
    				<td align="right" valign="top" class="topTxt"><a href="main.jsp" target="rightFrame">主页</a> | <a href="manager/changePwd.jsp"  target="rightFrame">修改密码</a> | <a href="">系统帮助</a> | <a href="/BusSystem/logServlet?method=out" target="_top" onclick="return confirm('确定要退出登录?')">退出登录</a> </td>
    			</tr>
    			<tr>
    			<td colspan="3" align="right" valign="bottom" style="font-size: 12px;font-weight: bold;padding-right:10px;">欢迎<span style="color: green;">${username}</span>登录</td>
    			</tr>
    		</table>
    		<hr />
    		</div>
</body>
</html>