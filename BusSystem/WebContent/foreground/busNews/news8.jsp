<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
   <c:set var="path" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>西安238路公交车进行线路调整 无人售票可刷卡-西安公交网</title>
	<meta name="description" content="5日记者从市公交总公司获悉，238路公交车将于8日进行线路调整。 具体调整由西华门缩线至张家堡西（盐张村），调整后仍由西安交大城市学院发车。站点设置为西安交大城市学院、尚-公交新闻">
	<meta name="keywords" content="西安238路公交车进行线路调整 无人售票可刷卡，公交，西安公交，西安公交查询，西安公交新闻">
	<meta name="applicable-device" content="pc">
	<meta name="location" content="province=陕西;city=西安;"> 
	<link href="${path }/foreground/bus/style/news_style.css" rel="stylesheet">
	<!-- <script type="text/javascript" src="http://cbjs.baidu.com/js/m.js"></script>
	 -->
</head>
<body>
	<div class="titlenav">
		<a href="javascript:void(0);">西安公交</a>  &gt;&gt;<a href="javascript:void(0);">西安公交新闻</a> &gt;&gt; 西安238路公交车进行线路调整 无人售票可刷卡
	</div>
    <div class="wrap clr">
    	<div class="left">
             <div class="info clr">
                <h1>西安238路公交车进行线路调整 无人售票可刷卡</h1>
                <p class="from">2017-02-21 &nbsp;&nbsp; 公交新闻</p>
                <pre> <p>5日记者从市公交总公司获悉，238路公交车将于8日进行线路调整。</p>
<p>具体调整由西华门缩线至张家堡西（盐张村），调整后仍由西安交大城市学院发车。站点设置为西安交大城市学院、尚稷路·草滩八路口、尚稷路·草滩七路口、尚稷路·草滩六路口、农场中站、尚稷路·草滩五路口、尚稷路·草滩三路口、尚稷路·草滩二路口、尚稷路·尚宏路口、农场东站、尚苑路·明光路口、西一村东、朱宏路公交枢纽站、三官庙、朱宏路·凤城九路口、文景西区南门、白桦林居、经开区管委会、城市运动公园、行政中心、张家堡、张家堡西（盐张村）。线路全长15.9公里，首班6:30，末班20:00，实行无人售票可刷卡。（记者张佳 实习生张敬慧）</p><p></p>
 </p>
</pre>
            </div>
            <div class="attention clr">
				<script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"0","bdSize":"32"},"share":{},"image":{"viewList":["qzone","tsina","tqq","renren","weixin"],"viewText":"分享到：","viewSize":"32"},"selectShare":{"bdContainerClass":null,"bdSelectMiniList":["qzone","tsina","tqq","renren","weixin"]}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>
			</div>
            <div class="pagetitle clr">
            	<div class="pre"><a href="news7.jsp" class="prea"  title="西安406路和256路公交调整 线路延伸3公里"> << 上一篇</a></div>
            	<div class="next">
            	<div class="next"><a href="news8.jsp" title="西安市7条公交线路有调整 正加速回购民营公交"  class="nexta">下一篇>></a></div>
           		</div>            			
            </div>
    	</div>
    	<div class="right">
    		<div class="public first clr">
    			<div class="title"><h2>西安公交资讯</h2></div>
    			<ul class="clr">
	   				<li><span class="first">1</span><a href="news1.jsp" target="_blank">西安市将开通浐灞4、5、6号公交30万市民受益</a></li>
	   				<li><span class="first">2</span><a href="news2.jsp" target="_blank">线路停运、车难等、卫生差 西安民营公交问题多</a></li>
	   				<li><span class="first">3</span><a href="news3.jsp" target="_blank">西安浐灞4、5、6号公交将开通 2号、3号线路有调</a></li>
	   				<li><span>4</span><a href="news4.jsp" target="_blank">西安将建63个公交枢纽站 解决公交马路停车问题</a></li>
	   				<li><span>5</span><a href="news1.jsp" target="_blank">西安民营公交17条线路被回购 退出导致行业垄断</a></li>
	   				<li><span>6</span><a href="news1.jsp" target="_blank">西安民营公交调查：夹缝求生 民营公交还能走多</a></li>
   				</ul>
    		</div>
    		<div class="public clr">
    			<div class="title"><h2>西安公交线路调整</h2></div>
    			<ul class="clr">
	   				<li><span class="first">1</span><a href="news6.jsp" target="_blank">西安公交近期新开7条线路 邀请市民参加调研</a></li>
	   				<li><span class="first">2</span><a href="news7.jsp" target="_blank">西安406路和256路公交调整 线路延伸3公里</a></li>
	   				<li><span class="first">3</span><a href="news8.jsp" target="_blank">西安238路公交车进行线路调整 无人售票可刷卡</a></li>
	   				<li><span>4</span><a href="news9.jsp" target="_blank">西安市7条公交线路有调整 正加速回购民营公交</a></li>
	   				<li><span>5</span><a href="news6.jsp" target="_blank">不开空调西安公交车票价依旧 物价部门:这是规定</a></li>
	    		    <li><span>6</span><a href="news6.jsp" target="_blank">29日起 西安市调整延伸406路和256路公交线路</a></li>
    			</ul>
    		</div>
    		<div class="ad last">
    		</div>
    	</div>
    </div>
    
    <div class="info">
	</div>
<c:import url="top.jsp"/>

<div class="footer">
	<div class="container">
		<p>
			西安翻译学院工程院电子系计算机科学与技术401班毕业设计   |   yangsen 版权所有
		</p>
	</div>
</div>
	<script type="text/javascript" src="${path }/foreground/bus/js/jquery-1.3.2.pack.js"></script>
	<script type="text/ecmascript" src="${path }/foreground/bus/js/city.js"></script>
</body>
</html>
