<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
           <%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
   <c:set var="path" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>西安浐灞4、5、6号公交将开通 2号、3号线路有调-西安公交网</title>
	<meta name="description" content="日前，记者从西安市交通局和浐灞管委会获悉，浐灞4号、5号、6号三条公交线路获批。 目前这三条线路的票价和站点审批都在同步进行，待条件成熟，尽早开通，方便区内居民出行。-公交新闻">
	<meta name="keywords" content="西安浐灞4、5、6号公交将开通 2号、3号线路有调，公交，西安公交，西安公交查询，西安公交新闻">
	<meta name="applicable-device" content="pc">
	<meta name="location" content="province=陕西;city=西安;"> 
	<link href="${path }/foreground/bus/style/news_style.css" rel="stylesheet">
	<!-- <script type="text/javascript" src="http://cbjs.baidu.com/js/m.js"></script> -->
	
</head>
<body>
	<div class="titlenav">
		<a href="javascript:void(0);">西安公交</a>  &gt;&gt;<a href="javascript:void(0);">西安公交新闻</a> &gt;&gt; 西安浐灞4、5、6号公交将开通 2号、3号线路有调
	</div>
    <div class="wrap clr">
    	<div class="left">
            <div class="info clr">
                  
  <h1>西安浐灞4、5、6号公交将开通 2号、3号线路有调</h1>
                <p class="from">2017-03-14 &nbsp;&nbsp; 公交新闻</p>
                <pre><p>&nbsp;日前，记者从西安市交通局和浐灞管委会获悉，浐灞4号、5号、6号三条公交线路获批。</p>
<p>&nbsp;目前这三条线路的票价和站点审批都在同步进行，待条件成熟，尽早开通，方便区内居民出行。</p>
<p>&nbsp;浐灞4号线：全长4.4公里，由浐灞中心地铁站始发，经东三环、环岛路、金茂一路、灞柳一路、浐河东路至桃花潭地铁站。</p>
<p>&nbsp;浐灞5号线：全长5.8公里，由广泰门地铁站始发，经广安路、北辰大道、浐灞二路、浐灞一路、广运潭大道至广泰门地铁站（顺时针环线）。</p>
<p>&nbsp;浐灞6号线：全长4.4公里，由浐河地铁站始发，经长乐路、沁水路、高楼路、浐河西路至浐河地铁站（逆时针环线）。</p>
<p>&nbsp;此外，浐灞旅游2号线调整为浐灞2号线，线路走向调整为：由世园旅汽基地始发，经港务大道、东三环、灞柳西路、金桥六路、金茂一路、浐河东路、欧亚大道、广运潭大道、浐灞二路、泘沱社区、浐灞一路、北辰大道至辛家庙公交枢纽站后，再经东二环至辛家庙地铁站，实现生态区与西安市主城区的连接，调整后线路全长14公里。</p>
<p>&nbsp;浐灞旅游3号线调整为浐灞3号线，线路走向调整为：由世博大道华夏文旅始发，经世博大道、东三环、灞柳西路、金茂五路、金桥一路、金茂四路、金桥三路、金茂七路、通塬路、浐河东路、御锦四路、御城路、长乐东路、沁水路、咸宁东路、浐河西路、东月路、花间路、月登阁村路、田马路至浐灞丝路学校，调整后线路全长25.5公里。记者 雷婧</p>
<p>&nbsp;编辑：</p>
<p> 李欣蔓（实习）</p>
</pre>
            </div>
            <div class="attention clr">
				<script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"0","bdSize":"32"},"share":{},"image":{"viewList":["qzone","tsina","tqq","renren","weixin"],"viewText":"分享到：","viewSize":"32"},"selectShare":{"bdContainerClass":null,"bdSelectMiniList":["qzone","tsina","tqq","renren","weixin"]}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>
			</div>
            <div class="pagetitle clr">
            	<div class="pre"><a href="news2.jsp" title="线路停运、车难等、卫生差 西安民营公交问题多" class="prea"> << 上一篇 </a></div>
            	<div class="next">
            	<div class="next"><a href="news4.jsp" title="西安将建63个公交枢纽站 解决公交马路停车问题"  class="nexta">下一篇>></a></div>
           		</div>            			
            </div>
    	</div>
    	<div class="right">
    		<div class="public first clr">
    			<div class="title"><h2>西安公交资讯</h2></div>
    			<ul class="clr">
	   				<li><span class="first">1</span><a href="news1.jsp" target="_blank">西安市将开通浐灞4、5、6号公交30万市民受益</a></li>
	   				<li><span class="first">2</span><a href="news2.jsp" target="_blank">线路停运、车难等、卫生差 西安民营公交问题多</a></li>
	   				<li><span class="first">3</span><a href="news3.jsp" target="_blank">西安浐灞4、5、6号公交将开通 2号、3号线路有调</a></li>
	   				<li><span>4</span><a href="news4.jsp" target="_blank">西安将建63个公交枢纽站 解决公交马路停车问题</a></li>
	   				<li><span>5</span><a href="news1.jsp" target="_blank">西安民营公交17条线路被回购 退出导致行业垄断</a></li>
	   				<li><span>6</span><a href="news1.jsp" target="_blank">西安民营公交调查：夹缝求生 民营公交还能走多</a></li>
   				</ul>
    		</div>
    		<div class="public clr">
    			<div class="title"><h2>西安公交线路调整</h2></div>
    			<ul class="clr">
	   				<li><span class="first">1</span><a href="news6.jsp" target="_blank">西安公交近期新开7条线路 邀请市民参加调研</a></li>
	   				<li><span class="first">2</span><a href="news7.jsp" target="_blank">西安406路和256路公交调整 线路延伸3公里</a></li>
	   				<li><span class="first">3</span><a href="news8.jsp" target="_blank">西安238路公交车进行线路调整 无人售票可刷卡</a></li>
	   				<li><span>4</span><a href="news9.jsp" target="_blank">西安市7条公交线路有调整 正加速回购民营公交</a></li>
	   				<li><span>5</span><a href="news6.jsp" target="_blank">不开空调西安公交车票价依旧 物价部门:这是规定</a></li>
	    		    <li><span>6</span><a href="news6.jsp" target="_blank">29日起 西安市调整延伸406路和256路公交线路</a></li>
    			</ul>
    		</div>
    		<div class="ad last">
    			<script type="text/javascript">BAIDU_CLB_fillSlot("481800");</script>
    		</div>
    	</div>
    </div>
    
    <div class="info">
		

 
	</div>
	
    

<c:import url="top.jsp"/>

<div class="footer">
	<div class="container">
		<p>
			西安翻译学院工程院电子系计算机科学与技术401班毕业设计   |   yangsen 版权所有
		</p>
	</div>
</div>
	<script type="text/javascript" src="${path }/foreground/bus/js/jquery-1.3.2.pack.js"></script>
	<script type="text/ecmascript" src="${path }/foreground/bus/js/city.js"></script>
</body>
</html>
