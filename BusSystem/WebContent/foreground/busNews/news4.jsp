<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
               <%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
   <c:set var="path" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>西安将建63个公交枢纽站 解决公交马路停车问题-西安公交网</title>
	<meta name="description" content="本报讯（记者赵丽莉）2月25日，本报以《西安五成公交车露宿街头赶快给公交车一个“家”》对公交车停车难的问题报道后引起了相关部门的重视。记者昨日获悉，西安将在三年内建设-公交新闻">
	<meta name="keywords" content="西安将建63个公交枢纽站 解决公交马路停车问题，公交，西安公交，西安公交查询，西安公交新闻">
	<meta name="applicable-device" content="pc">
	<meta name="location" content="province=陕西;city=西安;"> 
	<link href="${path }/foreground/bus/style/news_style.css" rel="stylesheet">
	<!-- <script type="text/javascript" src="http://cbjs.baidu.com/js/m.js"></script> -->
	
</head>
<body>
	<div class="titlenav">
		<a href="javascript:void(0);">西安公交</a>  &gt;&gt;<a href="javascript:void(0);">西安公交新闻</a> &gt;&gt; 西安将建63个公交枢纽站 解决公交马路停车问题
	</div>
    <div class="wrap clr">
    	<div class="left">
            <div class="info clr">
                <h1>西安将建63个公交枢纽站 解决公交马路停车问题</h1>
                <p class="from">2017-03-14 &nbsp;&nbsp; 公交新闻</p>
                <pre><p>本报讯（记者赵丽莉）2月25日，本报以《西安五成公交车露宿街头赶快给公交车一个“家”》对公交车停车难的问题报道后引起了相关部门的重视。记者昨日获悉，西安将在三年内建设23个公交保养站、55个停车场、63个枢纽站，解决公交车马路停车掉头问题，缓解道路通行压力。 </p><p>目前，西安市的公交进场率由2013年的32.4%提高到50.8%，但仍有超过五成的3900辆车露宿街头，给本就拥堵的城市道路带来更大的通行压力，起始站点的掉头也增加了路面通行的危险性。露天停车引发的车辆被盗、被破坏、火灾、甚至造成交通事故等现象，给公交车夜间巡逻看护带来诸多不便，加之私家车肆意占用车位，使得公交车停放“雪上加霜”。为此，人大代表、政协委员也多次为给公交车建一个“家”而鼓与呼。 </p><p>记者昨日获悉，西安市委、市政府对公交车停车难的问题特别重视，规划部门已经对此作出规划，从今年起到2020年，全市计划三年内将建设23个保养站、55个停车场、63个公交枢纽站，解决公交车停靠难、掉头难等系列难题。 </p>
正文已结束，您可以按alt+4进行评论
</pre>
            </div>
            <div class="attention clr">
				<script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"0","bdSize":"32"},"share":{},"image":{"viewList":["qzone","tsina","tqq","renren","weixin"],"viewText":"分享到：","viewSize":"32"},"selectShare":{"bdContainerClass":null,"bdSelectMiniList":["qzone","tsina","tqq","renren","weixin"]}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>
			</div>
            <div class="pagetitle clr">
            	<div class="pre"><a href="news3.jsp" title="西安浐灞4、5、6号公交将开通 2号、3号线路有调" class="prea"> << 上一篇 </a></div>
            	<div class="next">
            	<div class="next"><a href="news1.jsp" title="西安市将开通浐灞4、5、6号公交30万市民受益"  class="nexta"> 下一篇 >> </a></div>
           		</div>            			
            </div>
    	</div>
    	<div class="right">
    		<div class="public first clr">
    			<div class="title"><h2>西安公交资讯</h2></div>
    			<ul class="clr">
	   				<li><span class="first">1</span><a href="news1.jsp" target="_blank">西安市将开通浐灞4、5、6号公交30万市民受益</a></li>
	   				<li><span class="first">2</span><a href="news2.jsp" target="_blank">线路停运、车难等、卫生差 西安民营公交问题多</a></li>
	   				<li><span class="first">3</span><a href="news3.jsp" target="_blank">西安浐灞4、5、6号公交将开通 2号、3号线路有调</a></li>
	   				<li><span>4</span><a href="news4.jsp" target="_blank">西安将建63个公交枢纽站 解决公交马路停车问题</a></li>
	   				<li><span>5</span><a href="news1.jsp" target="_blank">西安民营公交17条线路被回购 退出导致行业垄断</a></li>
	   				<li><span>6</span><a href="news1.jsp" target="_blank">西安民营公交调查：夹缝求生 民营公交还能走多</a></li>
   				</ul>
    		</div>
    		<div class="public clr">
    			<div class="title"><h2>西安公交线路调整</h2></div>
    			<ul class="clr">
	   				<li><span class="first">1</span><a href="news6.jsp" target="_blank">西安公交近期新开7条线路 邀请市民参加调研</a></li>
	   				<li><span class="first">2</span><a href="news7.jsp" target="_blank">西安406路和256路公交调整 线路延伸3公里</a></li>
	   				<li><span class="first">3</span><a href="news8.jsp" target="_blank">西安238路公交车进行线路调整 无人售票可刷卡</a></li>
	   				<li><span>4</span><a href="news9.jsp" target="_blank">西安市7条公交线路有调整 正加速回购民营公交</a></li>
	   				<li><span>5</span><a href="news6.jsp" target="_blank">不开空调西安公交车票价依旧 物价部门:这是规定</a></li>
	    		    <li><span>6</span><a href="news6.jsp" target="_blank">29日起 西安市调整延伸406路和256路公交线路</a></li>
    			</ul>
    		</div>
    		<div class="ad last">
    			<script type="text/javascript">BAIDU_CLB_fillSlot("481800");</script>
    		</div>
    	</div>
    </div>
    
    <div class="info">
	</div>
<c:import url="top.jsp"/>

<div class="footer">
	<div class="container">
		<p>
			西安翻译学院工程院电子系计算机科学与技术401班毕业设计   |   yangsen 版权所有
		</p>
	</div>
</div>
	<script type="text/javascript" src="${path }/foreground/bus/js/jquery-1.3.2.pack.js"></script>
	<script type="text/ecmascript" src="${path }/foreground/bus/js/city.js"></script>
</body>
</html>
