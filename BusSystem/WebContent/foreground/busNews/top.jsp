<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<div class="twidth">
		<div class="topbar clr">
			<span><i class="homei"></i><a
				href="${path }/foreground/index.jsp">首页</a></span>
		</div>
	</div>


	<div class="nav clr">
		<a href="${path }/foreground/index.jsp" class="logo"><img
			src="${path }/foreground/img/logo2.gif" alt="系统logo" class="imglogo"></a>
		<div class="city">
			<span id="s_title"><a href="${path }/foreground/index.jsp"
				id="a_title"><font id="f_title">西安公交网</font></a></span> <a
				href="javascript:;" id="swith"></a> <input type="hidden"
				name="h_city" id="h_city" value="西安市" />
			<div class="citylist" id="cityList" style="display: none;">
				<a href="javascript:;" class="close" id="close"></a>


			</div>
		</div>
		<div class="navselect clr" id="searchBox">
		
			<div class="ts clr">
				<div class="ts clr">
				<form action="../transfer.jsp" method="post">
					<input type="text" placeholder="输入起点" class="black" id="on"><span>到</span>
					<input type="text" placeholder="输入终点" class="black end" id="dn">
					<a href="javascript:searchBus(1);">公交查询</a>
				</form>
				</div>
				<div class="ts clr" style="display: none;">
					<input type="text" placeholder="输入路线" class="line black"
						id="line_keyword"> <a href="javascript:searchBus(2);">公交查询</a>
				</div>
				<div class="ts clr" style="display: none;">
					<form action="/BusSystem/queryBusInfo" method="post">
						<input type="hidden" value="find" name="method" /> <input
							type="text" placeholder="输入站点" name="station"
							class="station black" id="station_keyword">
						<button id="astation">
							<a>公交查询</a>
						</button>
					</form>
				</div>
			</div>
			<div class="btab">
				<ul class="clr">
					<li class="search_nav_on" idx="0">公交换乘</li>
					<li idx="1">线路查询</li>
					<li idx="2">站点查询</li>
				</ul>
			</div>
		</div>
	</div>
</body>
</html>