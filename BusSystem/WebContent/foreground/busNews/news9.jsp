<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
               <%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
   <c:set var="path" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>西安市7条公交线路有调整 正加速回购民营公交</title>
	<meta name="keywords" content="西安市7条公交线路有调整 正加速回购民营公交，公交，西安公交，西安公交查询，西安公交新闻">
	<meta name="applicable-device" content="pc">
	<meta name="location" content="province=陕西;city=西安;"> 
	<link href="${path }/foreground/bus/style/news_style.css" rel="stylesheet">
	<!-- <script type="text/javascript" src="http://cbjs.baidu.com/js/m.js"></script> -->
	
</head>
<body>
	<div class="titlenav">
		<a href="javascript:void(0);">西安公交</a>  &gt;&gt;<a href="javascript:void(0);">西安公交新闻</a> &gt;&gt; 西安市7条公交线路有调整 正加速回购民营公交
	</div>
    <div class="wrap clr">
    	<div class="left">
               <div class="info clr">
                <h1>西安市7条公交线路有调整 正加速回购民营公交</h1>
                <p class="from">2017-02-14 &nbsp;&nbsp; 公交新闻</p>
                <pre> 发布时间：2017-02-11 12:12:17&nbsp;|&nbsp;
 来源：西部网&nbsp;|&nbsp;
 作者：佚名&nbsp;|&nbsp;
 责任编辑：DH001
 西安市交通运输局负责人就公交线路调整做介绍。西部网讯（记者刘望）近一段时间以来，西部网不断接到网友反映说，现在部分公交车比较难等，甚至有些线路突然消失了。2月10日下午，西 
 <p><br></p><p>西安市交通运输局负责人就公交线路调整做介绍。</p><p>西部网讯（记者 刘望）近一段时间以来，西部网不断接到网友反映说，现在部分公交车比较难等，甚至有些线路突然消失了。2月10日下午，西安市交通运输局召开座谈会，向市民代表和媒体解释了相关现象出现的原因。</p><p>政府正加大民营公交回购力度 等车难线路少将得到解决</p><p>据统计，西安市目前共有公交运营企业18家，公交线路270条，运营车辆7829辆，日客运量401万人次，出行分担率46.06%。2016年新开、调整公交线路47条，弥补了玄武路、太和路等路段公交空白，解决了新植物园、公园南路南段、朱雀蔬菜批发市场等区域市民出行和公交车辆马路停放掉头等问题。</p><p>西安市交通运输局负责人说，受城市规划或道路建设等因素的影响，目前在西安的一些新建城区、开发区和城市边缘等个别人口密度过小的地区会出现路网建设薄弱的情况，交通运输局正在协调处理，加大公交线路的新开、调整和优化工作。</p><p>对于部分市民反映的民营公交难等，个别线路消失等问题，交通运输局负责人表示，为提升交通服务质量，加大对市民的优惠政策，目前政府部门正在对民营公交进行回购工作。回购后的公交线路将由西安市公交总公司运营，以便降低市民出行成本。</p><p>目前已经收回了933路、710路等16条民营公交线路，部分线路正在调配新的小型公交车辆。西安市政府共投放了200辆新的小型公交车辆，目前有50辆已经到位，2月底厂家将到货70辆，三月中下旬200辆公交车将全部到齐，等车难或者线路消失等问题将得到解决。</p><p>市民反映强烈的7条线路近期出现调整 出行不再难</p><p>对于部分网友近期反映的公交线路上的问题，西安市交通运输局也及时给出了回复。公交部门表示，针对市民反映的问题，他们将认真进行梳理，尽最大力量解决相关问题，保障绝大多数市民的出行安全。</p><p>市民反映的东月路公交不便的问题，浐灞旅游3号线、172路已经过该路段，717路也进行了调整。待雁鸣湖公交站场充电桩建设完毕后，172路车还将更新成大型纯电动公交车。</p><p>市民反映的软件新城地区市民出行不便的问题，早晚高峰时段已经增开了4部区间车，相关部门加强了526路营运车辆的调配，缩短了发车时间间隔，原来5分钟车距已调整为3分钟，近期还将开通与地铁三号线接驳的公交线路。</p><p>市民反映的关于雅荷度假山庄小区出行不便的问题，自2月9日起公交509路已经绕行该小区。关于浐灞区线路和地铁站接驳与延点问题，近期还将开通浐灞区内连接地铁站的公交线路。</p><p>关于浐灞区线路和地铁站接驳与延点问题，近期还将开通浐灞区内连接地铁站的公交线路。为了西北一路、习武园地区市民出行，相关部门对703路公交车进行了调整，该公交线路将途经上述区域。</p><p>市民反映的大寨路西段公交车运力不足的问题，公交公司将于3月份增加3到5部253路营运车。部分市民反映的长安区通往主城区大公交线路不足的问题，公交公司将于3月1日起开通173路、延伸268路，等长安区民营公交车股份回购退出后，实现长安主城区大公交的全覆盖。</p><p></p> <p> 编辑： </p><p></p> 
</pre>
            </div>
            <div class="attention clr">
				<script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"0","bdSize":"32"},"share":{},"image":{"viewList":["qzone","tsina","tqq","renren","weixin"],"viewText":"分享到：","viewSize":"32"},"selectShare":{"bdContainerClass":null,"bdSelectMiniList":["qzone","tsina","tqq","renren","weixin"]}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>
			</div>
            <div class="pagetitle clr">
            	<div class="pre"><a href="news8.jsp" class="prea" title="西安238路公交车进行线路调整 无人售票可刷卡"  > << 上一篇</a></div>
            	<div class="next">
            	<div class="next"><a href="news1.jsp" title="西安公交近期新开7条线路 邀请市民参加调研"  class="nexta">下一篇>></a></div>
           		</div>            			
            </div>
    	</div>
    	<div class="right">
    		<div class="public first clr">
    			<div class="title"><h2>西安公交资讯</h2></div>
    			<ul class="clr">
	   				<li><span class="first">1</span><a href="news1.jsp" target="_blank">西安市将开通浐灞4、5、6号公交30万市民受益</a></li>
	   				<li><span class="first">2</span><a href="news2.jsp" target="_blank">线路停运、车难等、卫生差 西安民营公交问题多</a></li>
	   				<li><span class="first">3</span><a href="news3.jsp" target="_blank">西安浐灞4、5、6号公交将开通 2号、3号线路有调</a></li>
	   				<li><span>4</span><a href="news4.jsp" target="_blank">西安将建63个公交枢纽站 解决公交马路停车问题</a></li>
	   				<li><span>5</span><a href="news1.jsp" target="_blank">西安民营公交17条线路被回购 退出导致行业垄断</a></li>
	   				<li><span>6</span><a href="news1.jsp" target="_blank">西安民营公交调查：夹缝求生 民营公交还能走多</a></li>
   				</ul>
    		</div>
    		<div class="public clr">
    			<div class="title"><h2>西安公交线路调整</h2></div>
    			<ul class="clr">
	   				<li><span class="first">1</span><a href="news6.jsp" target="_blank">西安公交近期新开7条线路 邀请市民参加调研</a></li>
	   				<li><span class="first">2</span><a href="news7.jsp" target="_blank">西安406路和256路公交调整 线路延伸3公里</a></li>
	   				<li><span class="first">3</span><a href="news8.jsp" target="_blank">西安238路公交车进行线路调整 无人售票可刷卡</a></li>
	   				<li><span>4</span><a href="news9.jsp" target="_blank">西安市7条公交线路有调整 正加速回购民营公交</a></li>
	   				<li><span>5</span><a href="news6.jsp" target="_blank">不开空调西安公交车票价依旧 物价部门:这是规定</a></li>
	    		    <li><span>6</span><a href="news6.jsp" target="_blank">29日起 西安市调整延伸406路和256路公交线路</a></li>
    			</ul>
    		</div>
    		<div class="ad last">
    		</div>
    	</div>
    </div>
    
    <div class="info">
	</div>
<c:import url="top.jsp"/>

<div class="footer">
	<div class="container">
		<p>
			西安翻译学院工程院电子系计算机科学与技术401班毕业设计   |   yangsen 版权所有
		</p>
	</div>
</div>
	<script type="text/javascript" src="${path }/foreground/bus/js/jquery-1.3.2.pack.js"></script>
	<script type="text/ecmascript" src="${path }/foreground/bus/js/city.js"></script>
</body>
</html>
