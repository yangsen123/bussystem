<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
           <%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
   <c:set var="path" value="${pageContext.request.contextPath}"/>
<!DOCTYPE >
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>线路停运、车难等、卫生差 西安民营公交问题多</title>
    <meta name="description" content="从1月1日至3月10日，西安关于公交的投诉287条，其中涉及民营公交线路的占18.1%。投诉主要集中在“线路停运”、“管理服务较差”、“难等”、“票价高”、“卫生差”5个方面。 -公交新闻">
	<meta name="keywords" content="线路停运、车难等、卫生差 西安民营公交问题多，西安公交，西安公交查询，西安公交新闻">
	<meta name="applicable-device" content="pc">
	<meta name="location" content="province=陕西;city=西安;"> 
	<link href="${path }/foreground/bus/style/news_style.css" rel="stylesheet">
	<!-- <script type="text/javascript" src="http://cbjs.baidu.com/js/m.js"></script> -->
	
</head>
<body>
	<div class="titlenav">
		<a href="javascript:void(0);">西安公交</a>  &gt;&gt;<a href="javascript:void(0);">西安公交新闻</a> &gt;&gt; 线路停运、车难等、卫生差 西安民营公交问题多
	</div><br />
    <div class="wrap clr">
    	<div class="left">
            <div class="info clr">
                         <h1>线路停运、车难等、卫生差 西安民营公交问题多</h1>
                <p class="from">2017-03-14 &nbsp;&nbsp; 公交新闻</p>
                <pre>
 <p>从1月1日至3月10日，西安关于公交的投诉287条，其中涉及民营公交线路的占18.1%。投诉主要集中在“线路停运”、“管理服务较差”、“难等”、“票价高”、“卫生差”5个方面。</p>
<p>问题一：线路停运</p>
<p>案例1多条7字头民营公交线路停运</p>
<p>在各类民营公交线路的投诉咨询中，线路停运占总量的49.1%。记者从西安市交通运输局了解到，近年来因经营成本上涨等因素，西安民营公交企业经营困难，部分线路出现了停运情况。其中，721路于2014年4月停运，722路、729路于2015年4月停运，720路缩线、减车。为促进西安市公交行业健康发展，逐步改善市民出行条件，西安启动了民营公交线路回购工作，现在市内正常营运的民营公交有31条。目前，已对716路、726路、715路、720路，722路等线路进行回购，回购后，公交总公司根据各线路不同情况，对收回的线路进行优化调整，减少公交复线，减轻市区交通压力。</p>
<p>而对线路撤销后的空白路段，行业管理部门已督促市公交总公司结合市民出行需求，及时调整或开通新的公交线路，保障市民出行需求。</p>
<p>案例2718路停运后 新线路尚未规划</p>
<p>“718路车怎么就突然停运了，灞桥洪庆地区的居民出行实在不方便。”2016年，记者不断接到市民关于718路停运的投诉和咨询，多数来电者表示公交公司应及时规划新的路线，方便群众出行。</p>
<p>2016年11月30日，西安市交通运输管理处就此回函华商报称，718路已纳入回购范围，待回购工作完成，将全面整合线路资源，进一步完善优化公交线路。但华商报记者实地调查发现，截至目前，718路留下的真空地带尚无公交填补。今年3月2日中午，记者在灞桥区洪庆街道发现，因为公交线路少，街道上的电动三轮和电瓶车随处可见，很多市民出行都选择乘坐“黑三轮”，存在安全隐患。</p>
<p>回应：洪庆地区尚未规划新线路</p>
<p>3月3日，记者拨打96716咨询，工作人员称“洪庆地区尚未规划新线路”。</p>
<p>随后，记者从西安市交通运输管理处获悉，718路停运后，虽尚未规划新线路，但为保障洪庆地区居民乘车，公交公司先后两次分别增加了5辆213路车，目前共有25辆车正常运营。2月6日西安市交通运输局曾开过一次座谈会，确定先实地考察公交线路缺失地区，确定可以增加车辆后可让公交公司补充新线路。目前该方案的实施还在过渡当中，需要一个过程才能补充西安市的公交缺口，但新线路难开通的主要原因还是公交场站难找。</p>
<p>问题二：管理服务较差</p>
<p>案例不认爱心卡老年卡遭诟病</p>
<p>在涉及民营公交线路的投诉中，“管理服务较差”占总投诉量的24%。其中，投诉民营公交线路“不认军残证”、“不认爱心卡”、“不认老年卡”的内容最为集中。</p>
<p>2月28日，记者在武警医院站，咨询一辆702路公交能否使用爱心长安通卡时，驾驶员表示，702路只认“军残证”和爱心卡，持长安通老年卡不能免费。717路公交所属的西安市富达中巴汽车队工作人员称，该趟线路只接受持军残证的乘客免费乘车。而719路公交所属的陕西平安运输集团工作人员则表示，719路军残证、爱心长安通卡、长安通老年卡都不认。</p>
<p>回应：企业经营困难 负担不起免费服务</p>
<p>3月2日，记者从西安市交通运输局获悉，除了部分客运班线，所有公交线路都应对持有军残证、爱心长安通卡的乘客免费。工作人员特别提醒，除了“9”字开头的20趟客运班线外，其余民营公交线路均可使用军残证、爱心长安通卡。为何管理部门有明确规定，但很多民营公交线路仍不遵守呢？</p>
<p>“我们是自负盈亏，咋能承担得起呢？”西安某民营公交线路驾驶员称，民营公交多为纯民间资本投入，自负盈亏，不享受政府财政补贴，尤其近几年民营公交企业经营困难，不是不愿意，是负担不起免费服务。</p>
<p>问题三：卫生差</p>
<p>案例407路公交座椅黑渍多</p>
<p>近日，多位西安市民反映，407路公交不仅难等，而且卫生不好。</p>
<p>3月2日上午9时10分，记者在黄雁村站等候407路，9时40分车号为陕AG8074的407路驶入。在上车的乘客中，一名持有老年卡的老先生，刷卡后提示无效卡，老人询问司机后得知，该趟车不能用老年卡。</p>
<p>上车后，记者发现该车驾驶员右侧横向护栏被抬起，用白色塑料绳子捆在一旁的横向护栏上。乘客上车时想扶一把都会抓空，存在安全隐患。另外车内多个座位及靠背上明显有黑色污渍，甚至车厢地面地板翘起的地方，里面全是灰尘。</p>
<p>9时46分，记者随机登上另一辆车号为陕AG8042的407路公交车，发现车内座位及靠背上也有不少黑色污渍，而且车窗下沿和座位旁边的车体也都呈黑色。</p>
<p>回应：立即进行整改</p>
<p>3月3日，记者从西安通成客运有限责任公司获悉，407路从城西客运站开往省射击场，全长19公里，目前14辆车正常运营。</p>
<p>“好多司机都去公交公司应聘了，一些车辆放到场站但没有司机，毕竟大公交公司的福利和待遇比我们好。”一工作人员称，他们也在网上和车辆上发布了招聘司机的通知，但收效甚微。</p>
<p>关于司机不认老年卡的问题，其表示将加大对车队司机的宣讲力度，记者提出是否能更换刷卡机时，其称：“目前卡机太多，更换起来工作量太大，只要市民乘车时出示一下，司机都会认。”</p>
<p>另外关于该车辆卫生差和安全隐患的问题，工作人员表示尽快联系相关人员立即进行整改。</p>
<p>问题四：车少难等</p>
<p>案例“等四五十分钟 801路车才来”</p>
<p>投诉车难等的占投诉民营线路总量的14%。</p>
<p>2月27日，市民费女士称，从前一周开始发现801路公交车很难等，“从早七时等到八时多才等来一趟，晚上车距也非常长，搞得我最近上班老迟到。”市民张女士也称，801路车平时就是等二十多分钟，但最近几天几乎都要等四五十分钟。</p>
<p>3月1日上午11时，记者来到810路车起始站曲江池调度站，11时21分，一趟801路公交进站。随后，记者又等候了22分钟，第二趟801路公交才进站。</p>
<p>回应：缺员、车辆故障率高是难等主要原因</p>
<p>3月1日，记者从801路公交所属的西安通成客运有限责任公司二车队了获悉，该线路一般平峰车距在20分钟，遇到交通拥堵的情况，车距为30分钟。</p>
<p>“缺员和车辆故障维修率高，是造成车距比较大的主要原因。”该公司工作人员称，近年来，801路公交驾驶员缺员严重，缺员率常年在40%左右，“工作强度大，很辛苦，驾驶员每天工作时间基本在14个小时左右，很难招来人。”</p>
<p>西安一家民营公交线路工作人员称，民营企业不享受政府补贴，加上西安大公交和地铁覆盖越来越广，民营公交线路客流不断下滑。同时，2013年，政府对民营公交企业取消了燃油补贴，企业经营更为困难。</p>
<p>问题五：票价高</p>
<p>案例游10公交票价涨幅大</p>
<p>涉及民营公交线路票价的投诉，占总投诉量的12%。</p>
<p>2月14日，市民王先生反映，游10路公交车不按规定收费，规定的是三站以内0.5元，依次进位，从渭河电厂到运动公园应该收取1.5元，却收取了4.5元。还有市民反映称游10票价上调幅度过大，同样的路程原来3.5元现在却要5元。</p>
<p>回应：线路及收费明细已公布</p>
</pre>
            </div>
            <div class="attention clr">
				<script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"0","bdSize":"32"},"share":{},"image":{"viewList":["qzone","tsina","tqq","renren","weixin"],"viewText":"分享到：","viewSize":"32"},"selectShare":{"bdContainerClass":null,"bdSelectMiniList":["qzone","tsina","tqq","renren","weixin"]}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>
			</div>
            <div class="pagetitle clr">
            	<div class="pre"><a href="news1.jsp" class="prea"> << 上一篇</a></div>
            	<div class="next">
            	<div class="next"><a href="news3.jsp" title="西安浐灞4、5、6号公交将开通 2号、3号线路有调"  class="nexta">下一篇>></a></div>
           		</div>            			
            </div>
    	</div>
    	<div class="right">
    		<div class="public first clr">
    			<div class="title"><h2>西安公交资讯</h2></div>
    			<ul class="clr">
	   				<li><span class="first">1</span><a href="news1.jsp" target="_blank">西安市将开通浐灞4、5、6号公交30万市民受益</a></li>
	   				<li><span class="first">2</span><a href="news2.jsp" target="_blank">线路停运、车难等、卫生差 西安民营公交问题多</a></li>
	   				<li><span class="first">3</span><a href="news3.jsp" target="_blank">西安浐灞4、5、6号公交将开通 2号、3号线路有调</a></li>
	   				<li><span>4</span><a href="news4.jsp" target="_blank">西安将建63个公交枢纽站 解决公交马路停车问题</a></li>
	   				<li><span>5</span><a href="news1.jsp" target="_blank">西安民营公交17条线路被回购 退出导致行业垄断</a></li>
	   				<li><span>6</span><a href="news1.jsp" target="_blank">西安民营公交调查：夹缝求生 民营公交还能走多</a></li>
   				</ul>
    		</div>
    		<div class="public clr">
    			<div class="title"><h2>西安公交线路调整</h2></div>
    			<ul class="clr">
	   				<li><span class="first">1</span><a href="news6.jsp" target="_blank">西安公交近期新开7条线路 邀请市民参加调研</a></li>
	   				<li><span class="first">2</span><a href="news7.jsp" target="_blank">西安406路和256路公交调整 线路延伸3公里</a></li>
	   				<li><span class="first">3</span><a href="news8.jsp" target="_blank">西安238路公交车进行线路调整 无人售票可刷卡</a></li>
	   				<li><span>4</span><a href="news9.jsp" target="_blank">西安市7条公交线路有调整 正加速回购民营公交</a></li>
	   				<li><span>5</span><a href="news6.jsp" target="_blank">不开空调西安公交车票价依旧 物价部门:这是规定</a></li>
	    		    <li><span>6</span><a href="news6.jsp" target="_blank">29日起 西安市调整延伸406路和256路公交线路</a></li>
    			</ul>
    		</div>
    		<div class="ad last">
    			<script type="text/javascript">BAIDU_CLB_fillSlot("481800");</script>
    		</div>
    	</div>
    </div>
    <div class="info">
	</div>

<c:import url="top.jsp"/>

<div class="footer">
	<div class="container">
		<p>
			西安翻译学院工程院电子系计算机科学与技术401班毕业设计   |   yangsen 版权所有
		</p>
	</div>
</div>
	<script type="text/javascript" src="${path }/foreground/bus/js/jquery-1.3.2.pack.js"></script>
	<script type="text/ecmascript" src="${path }/foreground/bus/js/city.js"></script>
</body>
</html>
