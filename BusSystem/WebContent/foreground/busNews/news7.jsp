<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
          <%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
   <c:set var="path" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>西安406路和256路公交调整 线路延伸3公里-西安公交网</title>
	<meta name="description" content="为方便市民出行，经上级部门批准，西安市公交总公司将于2016年12月29日调整延伸406路和256路。 406路线路运行图 一、406路线路： 仍由常家湾发车，经半引路、半坡路、长乐路至半坡公-公交新闻">
	<meta name="keywords" content="西安406路和256路公交调整 线路延伸3公里，公交，西安公交，西安公交查询，西安公交新闻">
	<meta name="applicable-device" content="pc">
	<meta name="location" content="province=陕西;city=西安;"> 
	<link href="${path }/foreground/bus/style/news_style.css" rel="stylesheet">
	<!-- <script type="text/javascript" src="http://cbjs.baidu.com/js/m.js"></script> -->
	
</head>
<body>
	<div class="titlenav">
		<a href="javascript:void(0);">西安公交</a>  &gt;&gt;<a href="javascript:void(0);">西安公交新闻</a> &gt;&gt; 西安406路和256路公交调整 线路延伸3公里
	</div>
    <div class="wrap clr">
    	<div class="left">
            <div class="info clr">
                <h1>西安406路和256路公交调整 线路延伸3公里</h1>
                <p class="from">2017-02-21 &nbsp;&nbsp; 公交新闻</p>
                <pre><p>为方便市民出行，经上级部门批准，西安市公交总公司将于2016年12月29日调整延伸406路和256路。 </p><p> </p><p></p><p>406路线路运行图</p><p>一、406路线路：</p><p>仍由常家湾发车，经半引路、半坡路、长乐路至半坡公交枢纽站后，再经长乐路、 河东路、御锦一路、御城路、御锦四路、御浦路至御锦城公交枢纽站（回程：御锦城公交站发车，经御浦路、御锦四路、御城路、御锦一路、东三环至半坡公交枢纽站恢复原线路）。</p><p>406路站点设置为常家湾、南江村、鲸鱼沟、神鹿坊南、神鹿坊北、湾子村、红旗制动厂、三殿村、 河建材厂、穆将王村、半引路北口、石羊村、五星村、郭家滩、电力机械厂、半坡博物馆、半坡公交枢纽站、 河（去）、御锦一路东口（回）、御城路 御锦一路口、御城路 御锦二路口、御城路 御锦三路口、御城路 御锦四路口、御浦路 御锦四路口、御锦城公交枢纽站。</p><p>线路延伸3公里，线路全长14.6公里，首班6:30 末班19:30，仍实行无人售票可刷卡票价。</p><p>二、256路线路延伸</p><p> </p><p></p><p>256路运行图</p><p>256路仍由红旗枢纽站发车，经红旗西路、渭滨路、凤城八路、太华路、北二环、东二环、长乐路至 河后，再经长乐路、 河东路、御锦一路、御城路、御锦四路、御浦路至御锦城公交站。</p><p>256路站点设置为红旗西路枢纽站、红旗西站、红旗厂、徐家湾、五二四厂、徐家堡、三家庄、百花村、太华路 凤城三路口、余家寨、西铁小区、北二环 太华路立交、井上村、辛家庙西村、辛家庙公交枢纽站、辛家庙、矿山路口、中铁现代物流、石家街、胡家庙、东二环 长乐路口、轻工市场、公园北路、万寿路、东郊车管所、长乐坡、沁水新城、 河、御城路 御锦一路口、御城路 御锦二路口、御城路 御锦三路口、御城路 御锦四路口、御浦路 御锦四路口、御锦城公交枢纽站。</p><p>线路延伸3公里，线路全长21.3公里，首班6:30 末班19:30，仍实行无人售票可刷卡票价。</p><p></p><p></p><p></p><p>西安市公交总公司</p><p>2016年12月28日</p>

</pre>
            </div>
            <div class="attention clr">
				<script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"0","bdSize":"32"},"share":{},"image":{"viewList":["qzone","tsina","tqq","renren","weixin"],"viewText":"分享到：","viewSize":"32"},"selectShare":{"bdContainerClass":null,"bdSelectMiniList":["qzone","tsina","tqq","renren","weixin"]}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>
			</div>
            <div class="pagetitle clr">
            	<div class="pre"><a href="news6.jsp" class="prea" title="西安公交近期新开7条线路 邀请市民参加调研" > << 上一篇</a></div>
            	<div class="next">
            	<div class="next"><a href="news8.jsp" title="西安238路公交车进行线路调整 无人售票可刷卡"  class="nexta">下一篇>></a></div>
           		</div>            			
            </div>
    	</div>
    	<div class="right">
    		<div class="public first clr">
    			<div class="title"><h2>西安公交资讯</h2></div>
    			<ul class="clr">
	   				<li><span class="first">1</span><a href="news1.jsp" target="_blank">西安市将开通浐灞4、5、6号公交30万市民受益</a></li>
	   				<li><span class="first">2</span><a href="news2.jsp" target="_blank">线路停运、车难等、卫生差 西安民营公交问题多</a></li>
	   				<li><span class="first">3</span><a href="news3.jsp" target="_blank">西安浐灞4、5、6号公交将开通 2号、3号线路有调</a></li>
	   				<li><span>4</span><a href="news4.jsp" target="_blank">西安将建63个公交枢纽站 解决公交马路停车问题</a></li>
	   				<li><span>5</span><a href="news1.jsp" target="_blank">西安民营公交17条线路被回购 退出导致行业垄断</a></li>
	   				<li><span>6</span><a href="news1.jsp" target="_blank">西安民营公交调查：夹缝求生 民营公交还能走多</a></li>
   				</ul>
    		</div>
    		<div class="public clr">
    			<div class="title"><h2>西安公交线路调整</h2></div>
    			<ul class="clr">
	   				<li><span class="first">1</span><a href="news6.jsp" target="_blank">西安公交近期新开7条线路 邀请市民参加调研</a></li>
	   				<li><span class="first">2</span><a href="news7.jsp" target="_blank">西安406路和256路公交调整 线路延伸3公里</a></li>
	   				<li><span class="first">3</span><a href="news8.jsp" target="_blank">西安238路公交车进行线路调整 无人售票可刷卡</a></li>
	   				<li><span>4</span><a href="news9.jsp" target="_blank">西安市7条公交线路有调整 正加速回购民营公交</a></li>
	   				<li><span>5</span><a href="news6.jsp" target="_blank">不开空调西安公交车票价依旧 物价部门:这是规定</a></li>
	    		    <li><span>6</span><a href="news6.jsp" target="_blank">29日起 西安市调整延伸406路和256路公交线路</a></li>
    			</ul>
    		</div>
    		<div class="ad last">
    		</div>
    	</div>
    </div>
    
    <div class="info">
	</div>
<c:import url="top.jsp"/>

<div class="footer">
	<div class="container">
		<p>
			西安翻译学院工程院电子系计算机科学与技术401班毕业设计   |   yangsen 版权所有
		</p>
	</div>
</div>
	<script type="text/javascript" src="${path }/foreground/bus/js/jquery-1.3.2.pack.js"></script>
	<script type="text/ecmascript" src="${path }/foreground/bus/js/city.js"></script>
</body>
</html>
