<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
          <%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
   <c:set var="path" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>西安公交近期新开7条线路 邀请市民参加调研-西安公交网</title>
	<meta name="description" content="昨日，记者从西安市公交总公司了解到，西安近期将新开调整7条线路来填补部分区域的公交空白，邀请市民参与线路调整的调研和意见征集，对有价值的合理化意见和建议给予一定奖励-公交新闻">
	<meta name="keywords" content="西安公交近期新开7条线路 邀请市民参加调研，公交，西安公交，西安公交查询，西安公交新闻">
	<meta name="applicable-device" content="pc">
	<meta name="location" content="province=陕西;city=西安;"> 
	<link href="${path }/foreground/bus/style/news_style.css" rel="stylesheet">
	<!-- <script type="text/javascript" src="http://cbjs.baidu.com/js/m.js"></script> -->
	
</head>
<body>
	<div class="titlenav">
		<a href="javascript:void(0);">西安公交</a>  &gt;&gt;<a href="javascript:void(0);">西安公交新闻</a> &gt;&gt; 西安公交近期新开7条线路 邀请市民参加调研
	</div>
    <div class="wrap clr">
    	<div class="left">
            <div class="info clr">
                <h1>西安公交近期新开7条线路 邀请市民参加调研</h1>
                <p class="from">2017-03-14 &nbsp;&nbsp; 公交新闻</p>
                <pre><p> 昨日，记者从西安市公交总公司了解到，西安近期将新开调整7条线路来填补部分区域的公交空白，邀请市民参与线路调整的调研和意见征集，对有价值的合理化意见和建议给予一定奖励。同时自我加压，及时解决市民反映的出行难、乘车难等问题。</p><p> 西安市公交总公司近期将新开调整7条线路，填补西咸新区、高新区、长安区、港务区等新开发区域的公交线路空白。推迟地铁站点接驳公交线路收车时间，加强部分客流集中区域、大客流线路以及市民乘车需求比较集中的线路的运力配备，进一步满足市民乘车需求。</p><p> 该公司相关负责人说，围绕市民对公交线路开辟、站点调整的意见和建议，总公司将开展意见征询大赛，倾听市民心声，邀请市民参与线路调整的调研和意见征集，与专业部门现场查勘线路，对有价值的合理化意见和建议进行一定奖励，为线路调整提供参考依据。</p><p>(三秦都市报)</p>
正文已结束，您可以按alt+4进行评论
</pre>
            </div>
            <div class="attention clr">
				<script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"0","bdSize":"32"},"share":{},"image":{"viewList":["qzone","tsina","tqq","renren","weixin"],"viewText":"分享到：","viewSize":"32"},"selectShare":{"bdContainerClass":null,"bdSelectMiniList":["qzone","tsina","tqq","renren","weixin"]}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>
			</div>
            <div class="pagetitle clr">
            	<div class="pre"><a href="avascript:void(0);" class="prea"  onclick="alert('没有上一篇了');"> << 上一篇 </a></div>
            	<div class="next">
            	<div class="next"><a href="news7.jsp" title="西安406路和256路公交调整 线路延伸3公里"  class="nexta">下一篇>></a></div>
           		</div>            			
            </div>
    	</div>
    	<div class="right">
    		<div class="public first clr">
    			<div class="title"><h2>西安公交资讯</h2></div>
    			<ul class="clr">
	   				<li><span class="first">1</span><a href="news1.jsp" target="_blank">西安市将开通浐灞4、5、6号公交30万市民受益</a></li>
	   				<li><span class="first">2</span><a href="news2.jsp" target="_blank">线路停运、车难等、卫生差 西安民营公交问题多</a></li>
	   				<li><span class="first">3</span><a href="news3.jsp" target="_blank">西安浐灞4、5、6号公交将开通 2号、3号线路有调</a></li>
	   				<li><span>4</span><a href="news4.jsp" target="_blank">西安将建63个公交枢纽站 解决公交马路停车问题</a></li>
	   				<li><span>5</span><a href="news1.jsp" target="_blank">西安民营公交17条线路被回购 退出导致行业垄断</a></li>
	   				<li><span>6</span><a href="news1.jsp" target="_blank">西安民营公交调查：夹缝求生 民营公交还能走多</a></li>
   				</ul>
    		</div>
    		<div class="public clr">
    			<div class="title"><h2>西安公交线路调整</h2></div>
    			<ul class="clr">
	   				<li><span class="first">1</span><a href="news6.jsp" target="_blank">西安公交近期新开7条线路 邀请市民参加调研</a></li>
	   				<li><span class="first">2</span><a href="news7.jsp" target="_blank">西安406路和256路公交调整 线路延伸3公里</a></li>
	   				<li><span class="first">3</span><a href="news8.jsp" target="_blank">西安238路公交车进行线路调整 无人售票可刷卡</a></li>
	   				<li><span>4</span><a href="news9.jsp" target="_blank">西安市7条公交线路有调整 正加速回购民营公交</a></li>
	   				<li><span>5</span><a href="news6.jsp" target="_blank">不开空调西安公交车票价依旧 物价部门:这是规定</a></li>
	    		    <li><span>6</span><a href="news6.jsp" target="_blank">29日起 西安市调整延伸406路和256路公交线路</a></li>
    			</ul>
    		</div>
    		<div class="ad last">
    		</div>
    	</div>
    </div>
    
    <div class="info">
	</div>
<c:import url="top.jsp"/>

<div class="footer">
	<div class="container">
		<p>
			西安翻译学院工程院电子系计算机科学与技术401班毕业设计   |   yangsen 版权所有
		</p>
	</div>
</div>
	<script type="text/javascript" src="${path }/foreground/bus/js/jquery-1.3.2.pack.js"></script>
	<script type="text/ecmascript" src="${path }/foreground/bus/js/city.js"></script>
</body>
</html>
