<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
   <c:set var="path" value="${pageContext.request.contextPath}"/>
<!DOCTYPE>
<html>		
<head>
	<meta charset="utf-8">
	<title>西安公交线路查询</title>
	<meta name="applicable-device" content="pc">
	<meta name="location" content="province=陕西;city=西安;"> 
	<link rel="stylesheet" type="text/css" href="${path }/foreground/css/style.css"/>
	<link rel="stylesheet" href="${path }/foreground/css/line_style.css" >
</head>
<body>
<div class="poi-wrap clr">
		<div class="poileft">
			<div class="nav2">
		    	<a href="javascript:void(0);">公交</a> &gt;&gt;<a href="${path }/foreground/index.jsp ">西安公交查询</a> &gt;&gt;<a href="${path }/foreground/index.jsp">西安公交线路查询</a>
		    </div>  	
		 </div>
</div>
	<c:forEach var="bus" items="${bus }">
	<div class="poi-wrap clr">
		<div class="poileft">
			
	        <div class="poi-box">
	            <div class="publicBox">
	                <span class="frl">更新时间：2017-03-01</span><h1>西安${bus.bus_name}上行公交线路</h1>
	                <dl class="clr">
	                    <dt><img src="${path }/foreground/img/1122.jpg"  style="width: 230px"></dt>
	                    <dd>
	                        <ul class="clr">
	                            <li>西安${bus.bus_name}公交车</li>
	                            <li><span>运营时间:</span></li>
	                            <li><p>发车间隔：${bus.time_interval}分钟</p></li>
	                            <li><p>首班车时间:${bus.first_bus}</p></li>
	                            <li><p>末班车时间:${bus.last_bus}</p></li>
	                            <li><span>票价信息:${bus.carfare}</span></li>
	                        </ul>
	                    </dd>
	                </dl>
	            </div>
	        </div>

	       
	        <h2>西安${bus.bus_name}上行公交站牌</h2>
	        <div class="poi-box">
	            <div class="publicBox busSign">
	                <dl class="clr">
	                    <dt><strong> ${bus.bus_name} </strong>  </dt>
	                    <dd>
	                        <h3><b>${bus.first_station}</b>——<b>${bus.last_station}</b></h3>
	                        
			                	<a href="javascript:void(0);">返程路线</a>
			                
	                    </dd>
	                </dl>
	            </div>
	        </div>
	        <h2 style="display:inline;">${bus.bus_name} 所有公交车站</h2> (共${bus.stations_num}站)
	        
	       <c:set value="${ fn:split(bus.stop_name, ',') }" var="names" />
	        <div class="poi-box">
	            <div class="publicBox buslist">
	                <ul class="clr" id="scrollTr">
	                 <c:forEach items="${names }" var="name" varStatus="ss">
	               <li<c:choose>
	                 <c:when test="${ss.index==0 }"> class="first" </c:when>
	                 <c:when test="${ss.index+1==bus.stations_num }">class='last'</c:when>
	                </c:choose>><a   title='大唐芙蓉园南门'><span>${ss.index+1 }</span><em>${name}</em></a></li>
	       			 </c:forEach>
	               <!--  <li stationIndex="2"><a id='HCCCUGVURHRFF' href='javascript:void(0);'  title='曲江池遗址公园'><span>2</span><em>曲江池遗址公园站</em></a></li>
	                <li stationIndex="3"><a id='HCCFRHVURHHGR' href='javascript:void(0);'  title='曲江池西路中段'><span>3</span><em>曲江池西路中段站</em></a></li>
	                <li stationIndex="4"><a id='HCCHGCVURHFSD' href='javascript:void(0);'  title='曲江池西路南口'><span>4</span><em>曲江池西路南口站</em></a></li>
	                <li stationIndex="5"><a id='HCCGTHWURHEBV' href='javascript:void(0);'  title='曲江池南路西口'><span>5</span><em>曲江池南路西口站</em></a></li>
	                <li stationIndex="6"><a id='HCCEISWURHBUV' href='javascript:void(0);'  title='曲江水厂'><span>6</span><em>曲江水厂站</em></a></li>
	                <li stationIndex="7"><a id='HCCBIAVURGUHD' href='javascript:void(0);'  title='西安建设大厦'><span>7</span><em>西安建设大厦站</em></a></li>
	                <li stationIndex="8"><a id='HCBUWUXURGSJI' href='javascript:void(0);'  title='曲江国际会议中心'><span>8</span><em>曲江国际会议中心站</em></a></li>
	                <li stationIndex="9"><a id='HCBTEVWURGUEF' href='javascript:void(0);'  title='汇新路北口'><span>9</span><em>汇新路北口站</em></a></li>
	                <li stationIndex="10"><a id='HCBRCHVURGSFI' href='javascript:void(0);'  title='国展中心'><span>10</span><em>国展中心站</em></a></li>
	                <li class='last'><a id='HCBJUBVURGJGI' href='javascript:void(0);'  title='电视塔'><span>11</span><em>电视塔站</em></a></li> -->
					</ul>
	            </div>
	        </div>
	
	<div class="info">
 
	</div>


   	
</div>
  </div>
  </c:forEach>
<div class="footer">
	<div class="container">
		<p>
			 西安翻译学院工程院电子系计算机科学与技术401班毕业设计   |   yangsen 版权所有
		</p>
	</div>
</div>



	<script type="text/javascript" src="bus/js/city.js"></script>
	<script type="text/javascript" src="bus/js/header.js"></script>
</body>
</html>
