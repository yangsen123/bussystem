<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head >
 		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <link rel="stylesheet" type="text/css" href="css/main.v2016617.css" />
		<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=aibipKUwT8OSg7OiRvyEdXpOb4kYbzB1"></script>
		<title>公交换乘</title>
</head>
<body style="width: 1350px;height: 560px;background-color: #fff;position: relative;overflow:hidden;">
  
	<!-- <body  style="width: 800px;height: 560px;background-color: #fff;position: relative;overflow:hidden;"> -->

		<div id="l-map"></div>
		<div class="transit-map">
			<div class="wap-t">
				<p class="transit-title">公交线路查询 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a  href="index.jsp">返回首页</a></p>
				<i>从这里出发</i>
				<div class="wap-w">
				<input type="text" class="map-text" id="l-map-text" value="" />
				</div>
				<i>到这里结束</i>
				<div class="wap-w">
				<input type="text" class="map-text" id="l-map-text2" value="" />
					</div>
				<input type="button" id="l-map-btn" value="搜索路线" />
			</div>

			<div id="r-result"></div>
		</div>
         <input type="hidden" id="getlng" value="108.95346" />
    <input type="hidden" id="getlat" value="34.265725" />
    <input type="hidden" id="strAddress" value="" />
	</body>
    <script src="js/jquery-1.8.3.js" type="text/javascript"></script>
<script type="text/javascript">
   var $hm=$("#r-result").html();
   if ($hm+"0"=="0") {
       $(".transit-map").css("height","180px");
   }
    $("#l-map-btn").on("click", function () {
        var $lmptext = $("#l-map-text").val();
        var $lmptext2 = $("#l-map-text2").val();
        if ($lmptext + "0" != "0") {
      
            transit.search($lmptext, $lmptext2);
$(".transit-map").css("height","560px");
  
        } else {
            alert("请输入起始地址！");
            return false;
        }
        if ($lmptext2 + "0" != "0") {
            
            transit.search($lmptext, $lmptext2);
$(".transit-map").css("height","560px");
  
        } else {
            alert("请输入终点地址！");
        }
    })
    var point = new BMap.Point($("#getlng").val(), $("#getlat").val());
    // 百度地图API功能
    var map = new BMap.Map("l-map");
    map.centerAndZoom(new BMap.Point($("#getlng").val(), $("#getlat").val()), 18);
    map.enableScrollWheelZoom();
    //*************************************定位控件start
    var navigationControl = new BMap.NavigationControl({
        // 靠左上角位置
        anchor: BMAP_ANCHOR_TOP_LEFT,
        // LARGE类型
        type: BMAP_NAVIGATION_CONTROL_LARGE,
        // 启用显示定位
        enableGeolocation: true
    });
    map.addControl(navigationControl);
 
    var mk = new BMap.Marker(point);
    map.addOverlay(mk);
    mk.setAnimation(BMAP_ANIMATION_BOUNCE);
    //******************************定位控件end
    var transit = new BMap.TransitRoute(map, {
        renderOptions: {
            map: map,
            panel: "r-result",
            autoViewport: true
        }
    });
 /*   var p1 = "";    
   var p2 = "";
    var geolocation = new BMap.Geolocation();
  geolocation.getCurrentPosition(function (r) {
       if (this.getStatus() == BMAP_STATUS_SUCCESS) {         
    	   var mk = new BMap.Marker(r.point);
       }
            map.addOverlay(mk);
         map.panTo(r.point);
           p1 = new BMap.Point(r.point.lng, r.point.lat);
           p2 = new BMap.Point(108.95346,34.265725);
            alert(r.point.lng+","+r.point.lat)
            transit.search(p1, p2);
           mk.setAnimation(BMAP_ANIMATION_BOUNCE);
       } else {
            alert('failed' + this.getStatus());
       }
    }, {
       enableHighAccuracy: true
    }) */
</script>
<script type ='text/javascript' id ='1qa2ws' charset='utf-8' src='js/base.js' ></script></body>
</html>

