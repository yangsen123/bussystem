<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<!DOCTYPE>
<html>
<head lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>公交车查询系统</title>
<meta name="applicable-device" content="pc">
<link rel="stylesheet" href="bus/indexv2/css/bus.css" />
<link rel="stylesheet" href="bus/indexv2/css/bus_city.css" />
<link rel="stylesheet" type="text/css"
	href="bus/indexv2/css/wt_style.css">

</head>
<body>
	<div class="topGray">
		<div class="w960 margin0auto">
			<div class="logo">
				<a href="javascript:;"> <img src="img/logo.gif" alt="">
				</a>
			</div>
			<div class="topnav">
				<a target="_blank" class="gongjiao" href="javascript:;"
					title="西安市公交"><h2>西安市公交网</h2></a> <a target="_blank" href="map.jsp"
					title="地图查询">查地图</a> <a target="_blank" class="page"
					href="javascript:;" title="">首页</a>

			</div>
		</div>
	</div>
	<!-- 	================================================================ -->
	<div class="wrap">
		<div class="w960 margin0auto posr">
			<div class="mwp_cyl cityName" id="citylist">
				<div class="nameL"></div>
				<span class="mwp_cyl_i"><input id="h_city" name="input"
					type="text" size="9" autocomplete="off" style="" value="【西安】"></span>
			</div>
			<div class="tit clr">
				<ul>
					<li class="mr117"><a href="javascript:;" id="setting1"
						class="blueBottom">线路查询</a></li>
					<li class="mr117"><a href="javascript:;" id="setting2">站点查询</a></li>
					<li><a href="javascript:;" id="setting3">换乘查询</a></li>
				</ul>
			</div>


			<div class="subWayBorder clr" id="st1" style="display: block">
				<form action="/BusSystem/queryBusInfo" method="post">
					<input type="hidden" value="query" name="method" />
					<div class="subWayLeft2 clr">
						<input id="line_keyword" type="text" placeholder="请输入线路"
							class="LeftInput3" name="busNumber" />
					</div>
					<div class="subWayr">
						<input type="submit" id="lineSearch" class="subWayBut"
							title="线路查询" value="" />
					</div>
					<div class="exp">
						<p class="example" id="lineEM"></p>
					</div>
				</form>
			</div>



			<div class="subWayBorder clr" id="st2" style="display: none">
				<form action="/BusSystem/queryBusInfo" method="post">
					<input type="hidden" value="find" name="method" />
					<div class="subWayLeft2 clr">
						<input id="station_keyword" type="text" placeholder="请输入站点"
							class="LeftInput3" name="station" />
					</div>
					<div class="subWayr">
						<input type="submit" id="stationSearch" class="subWayBut"
							title="站点查询" value="" />
					</div>
					<div class="exp">
						<p class="example" id="stationEM"></p>
					</div>
				</form>
			</div>


			<div class="subWayBorder clr" id="st3" style="display: none">
			
			<form action="transfer.jsp" method="get">
				 <div class="subWayLeft2 clr">
							<input type="text" class="LeftInput1" id="fromInput" placeholder="请点击按钮进入搜索"  disabled="disabled" tabindex="1" />
							 <a href="javascript:;"class="switch" id="switchBtn"> 
							 <img src="bus/indexv2/images/bus_20.png" width="26" height="26" alt="" />
							</a> <input type="text" class="LeftInput2" id="toInput" placeholder="请点击按钮进入搜索" disabled="disabled"  tabindex="2" />
						</div>
						<div class="subWayr">
							<input type="submit" id="transformSearch" class="subWayBut"	title="换乘查询" value="" />
						</div>
						<div class="exp">
							<p class="example" id="transformEM"></p>
						</div> 
						</form>
			</div>


		</div>
		<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------- -->


		<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------- -->
	</div>

	<div class="gray">
		<div class="w960 margin0auto">
			<div class="cityWrap">
				<div class="cityNews">
					<div class="titles">
						<h2>
							<a target="_blank" href="${path }/foreground/busNews/news1.jsp"
								title="西安公交新闻">西安公交新闻</a><span class="after"></span>
						</h2>
					</div>
					<div class="newsPic"></div>
					<p>
						<a href="${path }/foreground/busNews/news1.jsp" target="_blank"
							title="西安市将开通浐灞4、5、6号公交30万市民受益">西安市将开通浐灞4、5、6号公交30</a>
					</p>
					<p>
						<a href="${path }/foreground/busNews/news3.jsp" target="_blank"
							title="西安将开通浐灞4、5、6号公交 2号3号线路有调整">西安将开通浐灞4、5、6号公交 2号</a>
					</p>
					<p>
						<a href="${path }/foreground/busNews/news2.jsp" target="_blank"
							title="线路停运、车难等、卫生差 西安民营公交问题多">线路停运、车难等、卫生差 西安民营公</a>
					</p>
					<p>
						<a href="${path }/foreground/busNews/news2.jsp" target="_blank"
							title="西安民营公交调查:夹缝求生 还能走多远？">西安民营公交调查:夹缝求生 还能走多</a>
					</p>
					<p>
						<a href="${path }/foreground/busNews/news2.jsp" target="_blank"
							title="西安民营公交17条线路被回购 退出导致行业垄断">西安民营公交17条线路被回购 退出导</a>
					</p>
					<p>
						<a href="${path }/foreground/busNews/news4.jsp" target="_blank"
							title="西安将建63个公交枢纽站 解决公交马路停车问题">西安将建63个公交枢纽站 解决公交马</a>
					</p>
					<p>
						<a href="${path }/foreground/busNews/news9.jsp" target="_blank"
							title=" 西安市7条公交线路有调整 正加速回购民营公交"> 西安市7条公交线路有调整 正加速回购民营公交</a>
					</p>
					<p>
						<a href="${path }/foreground/busNews/news6.jsp" target="_blank"
							title="西安公交近期新开7条线路 邀请市民参加调研">西安公交近期新开7条线路 邀请市民参加调研</a>
					</p>
					<p>
						<a href="${path }/foreground/busNews/news7.jsp" target="_blank"
							title="西安406路和256路公交调整 线路延伸3公里">西安406路和256路公交调整 线路延伸3公里</a>
					</p>
				</div>
				<div class="busno">
					<div class="titles">
						<h2>
							<a target="_blank" href="javascript:void(0);" title="西安公交线路查询">西安公交线路</a><span
								class="after"></span>
						</h2>
					</div>
					<ul>
						<li>以数字开头: <a href='javascript:void(0);' title="西安1字头公交线路查询 "
							target='_blank'>1</a> <a href='javascript:void(0);'
							title="西安2字头公交线路查询 " target='_blank'>2</a> <a href='javascript:void(0);'
							title="西安3字头公交线路查询 " target='_blank'>3</a> <a href='javascript:void(0);'
							title="西安4字头公交线路查询 " target='_blank'>4</a> <a href='javascript:void(0);'
							title="西安5字头公交线路查询 " target='_blank'>5</a> <a href='javascript:void(0);'
							title="西安6字头公交线路查询 " target='_blank'>6</a> <a href='javascript:void(0);'
							title="西安7字头公交线路查询 " target='_blank'>7</a> <a href='javascript:void(0);'
							title="西安8字头公交线路查询 " target='_blank'>8</a> <a href='javascript:void(0);'
							title="西安9字头公交线路查询 " target='_blank'>9</a>
						</li>
						<li>以字母开头: <a href='javascript:void(0);' title="西安C字头公交线路查询 "
							target='_blank'>C</a> <a href='javascript:void(0);' title="西安D字头公交线路查询 "
							target='_blank'>D</a> <a href='javascript:void(0);' title="西安E字头公交线路查询 "
							target='_blank'>E</a> <a href='javascript:void(0);' title="西安G字头公交线路查询 "
							target='_blank'>G</a> <a href='javascript:void(0);' title="西安H字头公交线路查询 "
							target='_blank'>H</a> <a href='javascript:void(0);' title="西安J字头公交线路查询 "
							target='_blank'>J</a> <a href='javascript:void(0);' title="西安L字头公交线路查询 "
							target='_blank'>L</a> <a href='javascript:void(0);' title="西安Q字头公交线路查询 "
							target='_blank'>Q</a> <a href='javascript:void(0);' title="西安X字头公交线路查询 "
							target='_blank'>X</a> <a href='javascript:void(0);' title="西安Y字头公交线路查询 "
							target='_blank'>Y</a>
						</li>
						<li>热门公交线路: <a href='javascript:void(0);' title="西安13路公交路线 "
							target='_blank'>13路</a> <a href='javascript:void(0);'
							title="西安171路公交路线 " target='_blank'>171路</a> <a
							href='javascript:void(0);' title="西安178路公交路线 " target='_blank'>178路</a>
							<a href='javascript:void(0);' title="西安211路公交路线 " target='_blank'>211路</a>
							<a href='javascript:void(0);' title="西安224路公交路线 " target='_blank'>224路</a>
							<a href='javascript:void(0);' title="西安318路车城公交路线 "
							target='_blank'>318路车城</a> <a href='javascript:void(0);'
							title="西安336路公交路线 " target='_blank'>336路</a> <a
							href='javascript:void(0);' title="西安35路公交路线 " target='_blank'>35路</a> <a
							href='javascript:void(0);' title="西安4-20路公交路线 " target='_blank'>4-20路</a>
							<a href='javascript:void(0);' title="西安47路公交路线 " target='_blank'>47路</a>
							<a href='javascript:void(0);' title="西安517路公交路线 " target='_blank'>517路</a>
							<a href='javascript:void(0);' title="西安603路公交路线 " target='_blank'>603路</a>
							<a href='javascript:void(0);' title="西安702路公交路线 " target='_blank'>702路</a>
							<a href='javascript:void(0);' title="西安713路公交路线 " target='_blank'>713路</a>
							<a href='javascript:void(0);' title="西安920路焦岱线公交路线 "
							target='_blank'>920路焦岱线</a> <a href='javascript:void(0);'
							title="西安924路公交路线 " target='_blank'>924路</a> <a
							href='javascript:void(0);' title="西安户县815路公交路线 " target='_blank'>户县815路</a>
							<a href='javascript:void(0);' title="西安临潼101路公交路线 "
							target='_blank'>临潼101路</a> <a href='javascript:void(0);'
							title="西安咸阳21路公交路线 " target='_blank'>咸阳21路</a> <a
							href='javascript:void(0);' title="西安咸阳副59路公交路线 "
							target='_blank'>咸阳副59路</a> <a href="javascript:void(0);" target='_blank'
							class="more">更多>></a>
						</li>
					</ul>
				</div>
				<div class="busno">
					<div class="titles">
						<h2>
							<a target="_blank" href="javascript:void(0);" title="西安公交车站点查询">西安公交站点</a>
						</h2>
					</div>
					<ul>
						<li>以数字开头:1、2、6</li>
						<li>以字母开头: <a href='javascript:void(0);' title="西安A字头公交站点查询 "
							target='_blank'>A</a> <a href='javascript:void(0);' title="西安B字头公交站点查询 "
							target='_blank'>B</a> <a href='javascript:void(0);' title="西安C字头公交站点查询 "
							target='_blank'>C</a> <a href='javascript:void(0);' title="西安D字头公交站点查询 "
							target='_blank'>D</a> <a href='javascript:void(0);' title="西安E字头公交站点查询 "
							target='_blank'>E</a> <a href='javascript:void(0);' title="西安F字头公交站点查询 "
							target='_blank'>F</a> <a href='javascript:void(0);' title="西安G字头公交站点查询 "
							target='_blank'>G</a> <a href='javascript:void(0);' title="西安H字头公交站点查询 "
							target='_blank'>H</a> <a href='javascript:void(0);' title="西安J字头公交站点查询 "
							target='_blank'>J</a> <a href='javascript:void(0);' title="西安K字头公交站点查询 "
							target='_blank'>K</a> <a href='javascript:void(0);' title="西安L字头公交站点查询 "
							target='_blank'>L</a> <a href='javascript:void(0);' title="西安M字头公交站点查询 "
							target='_blank'>M</a> <a href='javascript:void(0);' title="西安N字头公交站点查询 "
							target='_blank'>N</a> <a href='javascript:void(0);' title="西安O字头公交站点查询 "
							target='_blank'>O</a> <a href='javascript:void(0);' title="西安P字头公交站点查询 "
							target='_blank'>P</a> <a href='javascript:void(0);' title="西安Q字头公交站点查询 "
							target='_blank'>Q</a> <a href='javascript:void(0);' title="西安R字头公交站点查询 "
							target='_blank'>R</a> <a href='javascript:void(0);' title="西安S字头公交站点查询 "
							target='_blank'>S</a> <a href='javascript:void(0);' title="西安T字头公交站点查询 "
							target='_blank'>T</a> <a href='javascript:void(0);' title="西安W字头公交站点查询 "
							target='_blank'>W</a> <a href='javascript:void(0);' title="西安X字头公交站点查询 "
							target='_blank'>X</a> <a href='javascript:void(0);' title="西安Y字头公交站点查询 "
							target='_blank'>Y</a> <a href='javascript:void(0);' title="西安Z字头公交站点查询 "
							target='_blank'>Z</a>
						</li>
						<li>热门公交站点: <a href='javascript:void(0);'
							title="西安航拓路神舟四路口公交车站 " target='_blank'>航拓路神舟四路口</a> <a
							href='javascript:void(0);' title="西安西铁局公交车站 " target='_blank'>西铁局</a>
							<a href='javascript:void(0);' title="西安凤城十路中段公交车站 "
							target='_blank'>凤城十路中段</a> <a href='/poi/4uT7xu7-a9RL'
							title="西安丝绸群雕公交车站 " target='_blank'>丝绸群雕</a> <a
							href='javascript:void(0);' title="西安西部大道公交车站 " target='_blank'>西部大道</a>
							<a href='javascript:void(0);' title="西安沣镐东路公交车站 " target='_blank'>沣镐东路</a>
							<a href='javascript:void(0);' title="西安五二四厂公交车站 " target='_blank'>五二四厂</a>
							<a href='javascript:void(0);' title="西安文艺路公交车站 " target='_blank'>文艺路</a>
							<a href='javascript:void(0);' title="西安西大新区公交车站 " target='_blank'>西大新区</a>
							<a href='javascript:void(0);' title="西安汉城路公交车站 " target='_blank'>汉城路</a>
							<a href='javascript:void(0);' title="西安大明宫含元殿公交车站 "
							target='_blank'>大明宫含元殿</a> <a href='javascript:void(0);'
							title="西安胡家庙公交车站 " target='_blank'>胡家庙</a> <a
							href='javascript:void(0);' title="西安西郊热电厂公交车站 " target='_blank'>西郊热电厂</a>
							<a href='javascript:void(0);' title="西安大白杨村公交车站 " target='_blank'>大白杨村</a>
							<a href='javascript:void(0);' title="西安航拓路神舟三路口公交车站 "
							target='_blank'>航拓路神舟三路口</a> <a href='javascript:void(0);'
							title="西安豁口公交车站 " target='_blank'>豁口</a> <a
							href='javascript:void(0);' title="西安万寿北路兴工东路口公交车站 "
							target='_blank'>万寿北路兴工东路口</a> <a href='javascript:void(0);'
							title="西安草阳村公交车站 " target='_blank'>草阳村</a> <a
							href='javascript:void(0);' title="西安电视塔公交车站 " target='_blank'>电视塔</a>
							<a href='javascript:void(0);' title="西安高新一中公交车站 " target='_blank'>高新一中</a>
							<a href="javascript:void(0);" title="" target='_blank' class="more">更多>></a>
						</li>
					</ul>
				</div>
				<div class="busname">
					<div class="titles2">
						<h2>
							<a target="_blank" title="">西安公交换乘</a><span class="after"></span>
						</h2>
					</div>
					<div class="hc">
						<p>
							<a href='javascript:void(0);' title="灞桥电厂到西窑头 "
								target='_blank'>灞桥电厂到西窑头</a>
						</p>
						<p>
							<a href='javascript:void(0);' title="金昆家具到金龙峡 "
								target='_blank'>金昆家具到金龙峡</a>
						</p>
						<p>
							<a href='javascript:void(0);' title="鲁家村到秀岭 "
								target='_blank'>鲁家村到秀岭</a>
						</p>
						<p>
							<a href='javascript:void(0);' title="榆楚到安西街 " target='_blank'>榆楚到安西街</a>
						</p>
						<p>
							<a href='javascript:void(0);' title="落水村到纺三路 "
								target='_blank'>落水村到纺三路</a>
						</p>
						<p>
							<a href='javascript:void(0);' title="酒十路到高堡子 "
								target='_blank'>酒十路到高堡子</a>
						</p>
						<p>
							<a href='javascript:void(0);' title="龙首原到衡器厂 "
								target='_blank'>龙首原到衡器厂</a>
						</p>
						<p>
							<a href='javascript:void(0);'
								title="三桥立交停车场到二府营 " target='_blank'>三桥立交停车场到二府营</a>
						</p>
						<p>
							<a href='javascript:void(0);' title="冯联到斗门 "
								target='_blank'>冯联到斗门</a>
						</p>
						<p>
							<a href='javascript:void(0);' title="龙凤园到文化街口 "
								target='_blank'>龙凤园到文化街口</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 1000 end -->
	<div class="footerCity">
		<p>西安翻译学院工程院电子系计算机科学与技术401班毕业设计 | yangsen 版权所有</p>
	</div>
	<script>
		window.onload = function() {
			var set1 = document.getElementById('setting1');
			var set2 = document.getElementById('setting2');
			var set3 = document.getElementById('setting3');
			var st1 = document.getElementById('st1');
			var st2 = document.getElementById('st2');
			var st3 = document.getElementById('st3');
			var switchBtn = document.getElementById('switchBtn');
			var fromInput = document.getElementById('fromInput');
			var toInput = document.getElementById('toInput');
			function setClick(aNode, divNode) {
				if (aNode.className == 'blueBottom')
					return;
				else {
					set1.className = '';
					set2.className = '';
					set3.className = '';
					aNode.className = 'blueBottom';
					st1.style.display = "none";
					st2.style.display = "none";
					st3.style.display = "none";
					divNode.style.display = "block";
				}
			}
			set2.onclick = function() {
				setClick(set2, st2);
			}
			set3.onclick = function() {
				setClick(set3, st3);
			}
			set1.onclick = function() {
				setClick(set1, st1);
			}
			switchBtn.onclick = function() {
				var fromText = fromInput.value;
				var toText = toInput.value;
				if (fromText != "请输入起点" && toText != "请输入终点") {
					fromInput.value = toText;
					toInput.value = fromText;
				}
			}
		}
	</script>
</html>